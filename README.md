# MikuMikuViewer

#### 介绍

MikuMikuViewer 


![输入图片说明](%E5%9B%BE%E7%89%87%E6%A0%BC%E5%BC%8F/2022-01-04_220639.png)


专为MMDer设计的图片查看器，支持bmp,jpg,png,gif,dds,tga,psd图片格式

食用简单，拖拽图片到窗口即可，可自由调节大小，置顶显示，亦可用于显示绘画参考图

![输入图片说明](%E5%9B%BE%E7%89%87%E6%A0%BC%E5%BC%8F/2.gif)

欢迎食用！


#### 下载
[蓝奏云](https://ifwz.lanzouw.com/izuFdydp8ni)
 