﻿/*
 * 由SharpDevelop创建。
 * 用户： Administrator
 * 日期: 2022/1/4/周二
 * 时间: 20:33
 * 
 * 要改变这种模板请点击 工具|选项|代码编写|编辑标准头文件
 */
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using GDImageLibrary;
using Paloma;
using Zgke.MyImage.ImageFile;
using ZXing;
using ZXing.QrCode;
using CSharpWin_JD.CaptureImage;
using System.Threading;


namespace MikuMikuViewer
{
	/// <summary>
	/// Description of MainForm.
	/// </summary>
	public partial class MainForm : Form
	{
		
		[DllImport("user32.dll")]//拖动无窗体的控件
        public static extern bool ReleaseCapture();
        [DllImport("user32.dll")]
        public static extern bool SendMessage(IntPtr hwnd, int wMsg, int wParam, int lParam);
        public const int WM_SYSCOMMAND = 0x0112;
        public const int SC_MOVE = 0xF010;
        public const int HTCAPTION = 0x0002;
        
        
        [DllImport("USER32.DLL")]
		public static extern int SetWindowLong(IntPtr hWnd, int nIndex, int dwNewLong);
		[DllImport("USER32.DLL")]
		public static extern int GetWindowLong(IntPtr hWnd, int nIndex);
		public static int GWL_STYLE = -16;
		public static int WS_CHILD = 0x40000000;
		public static int WS_BORDER = 0x00800000;
		public static int WS_DLGFRAME = 0x00400000;
		public static int WS_CAPTION = WS_BORDER | WS_DLGFRAME;
        
		public MainForm()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			 this.pictureBox1.AllowDrop = true;
			 
			 int style = GetWindowLong(Handle, GWL_STYLE);
				SetWindowLong(Handle, GWL_STYLE, (style & ~WS_CAPTION));
				Height = ClientRectangle.Height;
				
		    //加载Susie插件
			this.manager = new SpiMngx.SpiManager(SpiMngx.LoadFlag.Load00IN
				| SpiMngx.LoadFlag.SusieDir | SpiMngx.LoadFlag.IfNeeded);		    

			//
			// TODO: Add constructor code after the InitializeComponent() call.
			//
		}
		void PictureBox1MouseDown(object sender, MouseEventArgs e)
		{
			//拖动窗体
            ReleaseCapture();
            SendMessage(this.Handle, WM_SYSCOMMAND, SC_MOVE + HTCAPTION, 0);
		}
		void PictureBox1DragOver(object sender, DragEventArgs e)
		{
			if ((e.AllowedEffect & DragDropEffects.Link) == DragDropEffects.Link)
			{
				e.Effect = DragDropEffects.Link;
			}
		}
		void PictureBox1DragDrop(object sender, DragEventArgs e)
		{
//			string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);
//			MessageBox.Show(files.GetValue(0).ToString());
			
			string[] array = e.Data.GetData("FileNameW") as string[];
			if (array.Length == 1)
			{
				string extension = Path.GetExtension(array[0]);
				if (!(extension == ".dds"))
				{
					if (!(extension == ".DDS"))
					{
						goto IL_5A;
					}
				}
				try
				{
					this.pictureBox1.Image = _DDS.LoadImage(array[0]);
					 
					return;
				}
				catch (Exception)
				{
					return;
				}
				IL_5A:
				if (!(extension == ".psd"))
				{
					if (!(extension == ".PSD"))
					{
						goto IL_93;
					}
				}
				try
				{
					ImagePsd imagePsd = new ImagePsd(array[0]);
					this.pictureBox1.Image = imagePsd.PSDImage;
					return;
				}
				catch (Exception)
				{
					return;
				}
				IL_93:
				if (extension == ".tga" || extension == ".TGA")
				{
					this.pictureBox1.Image = Paloma.TargaImage.LoadTargaImage(array[0]);
					return;
				}
				 
				this.pictureBox1.Image = Image.FromFile(array[0]);
				 
				
			}
		}
		void ExitToolStripMenuItemClick(object sender, EventArgs e)
		{
			this.Close();
			if (Directory.Exists(temp)) {
				Directory.Delete(temp,true);
			}
		}
		void MiniToolStripMenuItemClick(object sender, EventArgs e)
		{
			this.WindowState = FormWindowState.Minimized;
		}
		void MainFormSizeChanged(object sender, EventArgs e)
		{
			if (this.WindowState == FormWindowState.Minimized)
            {
                this.Hide();
                this.notifyIcon1.Visible = true;
            }
		}
		void NotifyIcon1MouseDoubleClick(object sender, MouseEventArgs e)
		{
			this.Visible = true;
            this.WindowState = FormWindowState.Normal;
            this.notifyIcon1.Visible = false;
            this.Activate();
            this.ShowInTaskbar = true;//任务栏区显示图标
		}
		string temp=Directory.GetCurrentDirectory()+"\\Temp";
		void ExitToolStripMenuItem1Click(object sender, EventArgs e)
		{
			this.Close();
			if (Directory.Exists(temp)) {
				Directory.Delete(temp,true);
			}
			
		}
		void AboutToolStripMenuItemClick(object sender, EventArgs e)
		{
			MessageBox.Show("Made by IFwz1729");
			System.Diagnostics.Process.Start("https://space.bilibili.com/30847042");
		}
		void HorizontalFlipToolStripMenuItemClick(object sender, EventArgs e)
		{
			FlipImage flip=new FlipImage();
			Image img = this.pictureBox1.Image;
			Bitmap map = new Bitmap(img);
			map=FlipImage.HorizontalFlip(map);
			pictureBox1.Image=map;
		}
		void VerticalFlipToolStripMenuItemClick(object sender, EventArgs e)
		{
			FlipImage flip=new FlipImage();
			Image img = this.pictureBox1.Image;
			Bitmap map = new Bitmap(img);
			map=FlipImage.VerticalFlip(map);
			pictureBox1.Image=map;
		}
		 
		void CWRotateToolStripMenuItemClick(object sender, EventArgs e)
		{
			FlipImage flip=new FlipImage();
			Image img = this.pictureBox1.Image;
			Bitmap map = new Bitmap(img);
			map=FlipImage.Rotate(map,90);
			pictureBox1.Image=map;
		}
		void CCWRotateToolStripMenuItemClick(object sender, EventArgs e)
		{
			FlipImage flip=new FlipImage();
			Image img = this.pictureBox1.Image;
			Bitmap map = new Bitmap(img);
			map=FlipImage.Rotate(map,-90);
			pictureBox1.Image=map;
		}
		void ToolStripMenuItem2Click(object sender, EventArgs e)
		{
			this.Opacity=0.8;
			int style = GetWindowLong(Handle, GWL_STYLE);
				SetWindowLong(Handle, GWL_STYLE, (style & ~WS_CAPTION));
				Height = ClientRectangle.Height;
		}
		void ToolStripMenuItem3Click(object sender, EventArgs e)
		{
			this.Opacity=1;
			int style = GetWindowLong(Handle, GWL_STYLE);
				SetWindowLong(Handle, GWL_STYLE, (style & ~WS_CAPTION));
				Height = ClientRectangle.Height;
		}
		void ToolStripMenuItem4Click(object sender, EventArgs e)
		{
			this.Opacity=0.6;
			int style = GetWindowLong(Handle, GWL_STYLE);
				SetWindowLong(Handle, GWL_STYLE, (style & ~WS_CAPTION));
				Height = ClientRectangle.Height;
		}
		void ToolStripMenuItem5Click(object sender, EventArgs e)
		{
			this.Opacity=0.4;
			int style = GetWindowLong(Handle, GWL_STYLE);
				SetWindowLong(Handle, GWL_STYLE, (style & ~WS_CAPTION));
				Height = ClientRectangle.Height;
		}
		int num=0;
		void FullscreenToolStripMenuItemClick(object sender, EventArgs e)
		{
			if (num%2==0) {
				this.WindowState = FormWindowState.Maximized;
				fullscreenToolStripMenuItem.Text="退出全屏";
				num++;
			}
			else{
				this.WindowState = FormWindowState.Normal;
				fullscreenToolStripMenuItem.Text="全屏";
				num++;
			}
			
		}
		void QcodeToolStripMenuItemClick(object sender, EventArgs e)
		{
			if (Clipboard.GetText()!="") {
				pictureBox1.Image=CreateQcode(Clipboard.GetText(),pictureBox1.Width,pictureBox1.Height);
			}
		}
		 
		       /// <summary>
       /// 生成二维码
       /// </summary>
       /// <param name="text">内容</param>
       /// <param name="width">宽度</param>
       /// <param name="height">高度</param>
       /// <returns></returns>
        public static Bitmap CreateQcode(string text,int width,int height)
        {
            BarcodeWriter writer = new BarcodeWriter();
            writer.Format = BarcodeFormat.QR_CODE;
            QrCodeEncodingOptions options = new QrCodeEncodingOptions()
            {
                DisableECI = true,//设置内容编码
                CharacterSet = "UTF-8",  //设置二维码的宽度和高度
                Width = width,
                Height = height,
                Margin = 1//设置二维码的边距,单位不是固定像素
            };

            writer.Options = options;
            Bitmap map = writer.Write(text);
            return map;
        }
		void CaptureToolStripMenuItemClick(object sender, EventArgs e)
		{
			this.Visible = false;
            System.Threading.Thread.Sleep(500);
            CaptureImageTool capture = new CaptureImageTool();
            if (capture.ShowDialog() == DialogResult.OK)
            {
                Image image = capture.Image;
                pictureBox1.Image = image;
                this.Visible = true;
            }
            else{
            	this.Visible = true;
            }
		}
		void SaveimageToolStripMenuItemClick(object sender, EventArgs e)
		{
			SaveFileDialog savedialog = new SaveFileDialog();
            savedialog.Filter = "Jpg 图片|*.jpg|Bmp 图片|*.bmp|Gif 图片|*.gif|Png 图片|*.png|Sph 图片|*.sph|Spa 图片|*.spa";
            savedialog.FilterIndex = 0;
            savedialog.RestoreDirectory = true;//保存对话框是否记忆上次打开的目录
            savedialog.CheckPathExists = true;//检查目录
            savedialog.FileName = System.DateTime.Now.ToString("yyyyMMddHHmmss"); ;//设置默认文件名
            if (savedialog.ShowDialog() == DialogResult.OK)
            {
            	Image image = pictureBox1.Image;
              image.Save  (savedialog.FileName, System.Drawing.Imaging.ImageFormat.Jpeg);// image为要保存的图片
                
            }
		}
		void NostalgiaToolStripMenuItemClick(object sender, EventArgs e)
		{
			Filter fi=new Filter();
			Image img = this.pictureBox1.Image;
			Bitmap map = new Bitmap(img);
			pictureBox1.Image=fi.NostalgiaFilter(map);
		}
		void BlackAndWhiteToolStripMenuItemClick(object sender, EventArgs e)
		{
			Filter fi=new Filter();
			Image img = this.pictureBox1.Image;
			Bitmap map = new Bitmap(img);
			pictureBox1.Image=fi.BlackAndWhiteFilter(map);
		}
		void ShadowToolStripMenuItemClick(object sender, EventArgs e)
		{
			Filter fi=new Filter();
			Image img = this.pictureBox1.Image;
			Bitmap map = new Bitmap(img);
			pictureBox1.Image=fi.ShadowFilter(map);
		}
		void OilpaintToolStripMenuItemClick(object sender, EventArgs e)
		{
			Filter fi=new Filter();
			Image img = this.pictureBox1.Image;
			Bitmap map = new Bitmap(img);
			pictureBox1.Image=fi.OilpaintFilter(map,5);
		}
		void CartoonToolStripMenuItemClick(object sender, EventArgs e)
		{
			Filter fi=new Filter();
			Image img = this.pictureBox1.Image;
			Bitmap map = new Bitmap(img);
			pictureBox1.Image=fi.CartoonFilter(map,5);
		}
		void LOMOFilterToolStripMenuItemClick(object sender, EventArgs e)
		{
			Filter fi=new Filter();
			Image img = this.pictureBox1.Image;
			Bitmap map = new Bitmap(img);
			pictureBox1.Image=fi.LOMOFilter(map);
		}
		void InstagramToolStripMenuItemClick(object sender, EventArgs e)
		{
			Filter fi=new Filter();
			Image img = this.pictureBox1.Image;
			Bitmap map = new Bitmap(img);
			pictureBox1.Image=fi.Instagram1977(map);
		}
		void FromclipboardToolStripMenuItemClick(object sender, EventArgs e)
		{
			if (Clipboard.GetImage()!=null) {
				pictureBox1.Image=Clipboard.GetImage();
			}
		}
		private SpiMngx.SpiManager manager;
		private string imagePath = "";
		void SusieToolStripMenuItemClick(object sender, EventArgs e)
		{
			this.manager = new SpiMngx.SpiManager(SpiMngx.LoadFlag.Load00IN
				| SpiMngx.LoadFlag.SusieDir | SpiMngx.LoadFlag.IfNeeded);
			
			try
			{
				// 僼傽僀儖慖戰僟僀傾儘僌梡偺僼傽僀儖僼傿儖僞傪嶌惉
				string fileFilter = this.manager.GetFileFilter();

				// 僼傽僀儖慖戰僟僀傾儘僌
				OpenFileDialog dlg = new OpenFileDialog();
				dlg.Filter = fileFilter;
				if (dlg.ShowDialog() != DialogResult.OK)
					return;

				// 僾儔僌僀儞偺懳墳妋擣
				SpiMngx.ImagePlugin spi = this.manager.QueryImagePlugin(dlg.FileName);
				if (spi == null)
					return;

				// 夋憸傪昞帵
				pictureBox1.Image = spi.GetPicture(dlg.FileName);
				this.imagePath = dlg.FileName;
				
 
			}
			catch (System.Exception exception)
			{
				MessageBox.Show(exception.Message, exception.Source);
			}
		}
		private SpiMngx.FileInfoCollection infoCol = null;
        private string arcPath = "";
		void SusieArchiveToolStripMenuItemClick(object sender, EventArgs e)
		{
			 
			
			this.manager = new SpiMngx.SpiManager(SpiMngx.LoadFlag.Load00AM
				| SpiMngx.LoadFlag.SusieDir | SpiMngx.LoadFlag.IfNeeded);
			
			 try
            {
                // 僼傽僀儖慖戰僟僀傾儘僌梡偺僼傽僀儖僼傿儖僞傪嶌惉
                string fileFilter = this.manager.GetFileFilter();

                // 僼傽僀儖慖戰僟僀傾儘僌
                OpenFileDialog dlg = new OpenFileDialog();
                dlg.Filter = fileFilter;
                if (dlg.ShowDialog() != DialogResult.OK)
                    return;

				// 彂屔僼傽僀儖柤傪婰壇偟偰偍偔
				this.arcPath = dlg.FileName;
				this.Text = "Sample - " + System.IO.Path.GetFileName(this.arcPath);
 
				// 僾儔僌僀儞偺懳墳妋擣
                SpiMngx.ArchivePlugin spi = this.manager.QueryArchivePlugin(this.arcPath);
                if (spi == null)
                    return;

                // 彂屔撪偺僼傽僀儖傪楍嫇  
				
                this.infoCol = spi.GetArchiveInfo(this.arcPath);
                
                string destPath	=Directory.GetCurrentDirectory()+"\\Temp";
                Directory.CreateDirectory(destPath);
				File.SetAttributes(destPath, FileAttributes.Hidden); 
				
				if (Directory.GetFiles(destPath).Length > 0){
					Directory.Delete(destPath,true);
					Directory.CreateDirectory(destPath);
				File.SetAttributes(destPath, FileAttributes.Hidden); 
				}
                
		
                foreach (SpiMngx.FileInfo info in infoCol)
                {
                	
					spi.GetFile(this.arcPath, info,
						System.IO.Path.Combine(destPath, info.FileName));
                }
                
                if (Directory.GetFiles(destPath).Length > 0){
					 DirectoryInfo folder = new DirectoryInfo(destPath);
					 List<String> list =new List<string>();
					foreach (FileInfo file in folder.GetFiles("*.*"))
					{
						list.Add(file.FullName);
					}
					imglist=list;
					string[] pic = list.ToArray();
	                Image img = Image.FromFile(pic[0]);
	                Bitmap bmp=new Bitmap(img);
	                pictureBox1.Image=bmp;
	                img.Dispose();
				}
               
            }
            catch (System.Exception exception)
            {
                MessageBox.Show(exception.Message, exception.Source);
            }
		}
		public  List<String> imglist;
		int i=0;
		void MainFormKeyDown(object sender, KeyEventArgs e)
		{	try {
			string[] pic = imglist.ToArray();
			if (e.KeyCode == Keys.Right)
			{
				if (pic.Length>0 && i<pic.Length-1) {
					 i++;
					Image img = Image.FromFile(pic[i]);
	                Bitmap bmp=new Bitmap(img);
	                pictureBox1.Image=bmp;
	                img.Dispose();
				}   
			}else if (e.KeyCode == Keys.Left) {
			
				if (pic.Length>0 ) {
					i--;
					i=System.Math.Abs(i); 
					Image img = Image.FromFile(pic[i]);
	                Bitmap bmp=new Bitmap(img);
	                pictureBox1.Image=bmp;
	                img.Dispose();
	                 
				}
			} 
			 
		} catch (Exception) {
			
			//throw;
		}
			
			 
			
		}
		int clockflag=0;
		void SlideplayerToolStripMenuItemClick(object sender, EventArgs e)
		{
			if (clockflag%2==0) {
				timer1.Enabled=true;
				slideplayerToolStripMenuItem.Text="暂停播放";
				clockflag++;
			}
			else{
				timer1.Enabled=false;
				slideplayerToolStripMenuItem.Text="幻灯播放";
				clockflag++;
			}
			
		}
		int clock=0;
		void Timer1Tick(object sender, EventArgs e)
		{
			try {
				string[] pic = imglist.ToArray();
				if (Directory.Exists(temp)) {
					
					if(clock < pic.Length) {
				 	Image img = Image.FromFile(pic[clock]);
	                Bitmap bmp=new Bitmap(img);
	                pictureBox1.Image=bmp;
	                img.Dispose();
	                clock++;
				 }		
					
				}
			} catch (Exception) {
				
				//throw;
			}
		}
		void FirstimageToolStripMenuItemClick(object sender, EventArgs e)
		{
			string[] pic = imglist.ToArray();
			Image img = Image.FromFile(pic[0]);
	                Bitmap bmp=new Bitmap(img);
	                pictureBox1.Image=bmp;
	                img.Dispose();	 
	                i=0;
		}
		 
		 
		//*******************************
		 
    
	}
}
