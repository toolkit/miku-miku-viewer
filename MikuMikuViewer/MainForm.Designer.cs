﻿/*
 * 由SharpDevelop创建。
 * 用户： Administrator
 * 日期: 2022/1/4/周二
 * 时间: 20:33
 * 
 * 要改变这种模板请点击 工具|选项|代码编写|编辑标准头文件
 */
namespace MikuMikuViewer
{
	partial class MainForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		private System.Windows.Forms.PictureBox pictureBox1;
		private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
		private System.Windows.Forms.ToolStripMenuItem miniToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
		private System.Windows.Forms.NotifyIcon notifyIcon1;
		private System.Windows.Forms.ContextMenuStrip contextMenuStrip2;
		private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem1;
		private System.Windows.Forms.ToolStripMenuItem horizontalFlipToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem verticalFlipToolStripMenuItem;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
		private System.Windows.Forms.ToolStripMenuItem cCWRotateToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem cWRotateToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem opacityToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
		private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem3;
		private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem4;
		private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem5;
		private System.Windows.Forms.ToolStripMenuItem fullscreenToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem qcodeToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem captureToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem saveimageToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem filterToolStripMenuItem;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
		private System.Windows.Forms.ToolStripMenuItem nostalgiaToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem blackAndWhiteToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem shadowToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem oilpaintToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem cartoonToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem lOMOFilterToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem instagramToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem fromclipboardToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem susieToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem susieArchiveToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem slideplayerToolStripMenuItem;
		private System.Windows.Forms.Timer timer1;
		private System.Windows.Forms.ToolStripMenuItem firstimageToolStripMenuItem;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
			this.pictureBox1 = new System.Windows.Forms.PictureBox();
			this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.captureToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.qcodeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.fromclipboardToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.filterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.lOMOFilterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.instagramToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.nostalgiaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.blackAndWhiteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.shadowToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.cartoonToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.oilpaintToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.susieToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.susieArchiveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.firstimageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.slideplayerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
			this.cWRotateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.cCWRotateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.horizontalFlipToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.verticalFlipToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.opacityToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripMenuItem5 = new System.Windows.Forms.ToolStripMenuItem();
			this.fullscreenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
			this.saveimageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.miniToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
			this.contextMenuStrip2 = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.exitToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
			this.timer1 = new System.Windows.Forms.Timer(this.components);
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
			this.contextMenuStrip1.SuspendLayout();
			this.contextMenuStrip2.SuspendLayout();
			this.SuspendLayout();
			// 
			// pictureBox1
			// 
			this.pictureBox1.ContextMenuStrip = this.contextMenuStrip1;
			this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
			this.pictureBox1.Location = new System.Drawing.Point(0, 0);
			this.pictureBox1.Name = "pictureBox1";
			this.pictureBox1.Size = new System.Drawing.Size(261, 261);
			this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
			this.pictureBox1.TabIndex = 0;
			this.pictureBox1.TabStop = false;
			this.pictureBox1.DragDrop += new System.Windows.Forms.DragEventHandler(this.PictureBox1DragDrop);
			this.pictureBox1.DragOver += new System.Windows.Forms.DragEventHandler(this.PictureBox1DragOver);
			this.pictureBox1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.PictureBox1MouseDown);
			// 
			// contextMenuStrip1
			// 
			this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
			this.captureToolStripMenuItem,
			this.qcodeToolStripMenuItem,
			this.fromclipboardToolStripMenuItem,
			this.filterToolStripMenuItem,
			this.susieToolStripMenuItem,
			this.susieArchiveToolStripMenuItem,
			this.firstimageToolStripMenuItem,
			this.slideplayerToolStripMenuItem,
			this.toolStripSeparator2,
			this.cWRotateToolStripMenuItem,
			this.cCWRotateToolStripMenuItem,
			this.horizontalFlipToolStripMenuItem,
			this.verticalFlipToolStripMenuItem,
			this.opacityToolStripMenuItem,
			this.fullscreenToolStripMenuItem,
			this.toolStripSeparator1,
			this.saveimageToolStripMenuItem,
			this.miniToolStripMenuItem,
			this.exitToolStripMenuItem});
			this.contextMenuStrip1.Name = "contextMenuStrip1";
			this.contextMenuStrip1.Size = new System.Drawing.Size(203, 390);
			// 
			// captureToolStripMenuItem
			// 
			this.captureToolStripMenuItem.Name = "captureToolStripMenuItem";
			this.captureToolStripMenuItem.Size = new System.Drawing.Size(202, 22);
			this.captureToolStripMenuItem.Text = "截屏";
			this.captureToolStripMenuItem.Click += new System.EventHandler(this.CaptureToolStripMenuItemClick);
			// 
			// qcodeToolStripMenuItem
			// 
			this.qcodeToolStripMenuItem.Name = "qcodeToolStripMenuItem";
			this.qcodeToolStripMenuItem.Size = new System.Drawing.Size(202, 22);
			this.qcodeToolStripMenuItem.Text = "剪贴板生成二维码";
			this.qcodeToolStripMenuItem.Click += new System.EventHandler(this.QcodeToolStripMenuItemClick);
			// 
			// fromclipboardToolStripMenuItem
			// 
			this.fromclipboardToolStripMenuItem.Name = "fromclipboardToolStripMenuItem";
			this.fromclipboardToolStripMenuItem.Size = new System.Drawing.Size(202, 22);
			this.fromclipboardToolStripMenuItem.Text = "从剪贴板获取图片";
			this.fromclipboardToolStripMenuItem.Click += new System.EventHandler(this.FromclipboardToolStripMenuItemClick);
			// 
			// filterToolStripMenuItem
			// 
			this.filterToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
			this.lOMOFilterToolStripMenuItem,
			this.instagramToolStripMenuItem,
			this.nostalgiaToolStripMenuItem,
			this.blackAndWhiteToolStripMenuItem,
			this.shadowToolStripMenuItem,
			this.cartoonToolStripMenuItem,
			this.oilpaintToolStripMenuItem});
			this.filterToolStripMenuItem.Name = "filterToolStripMenuItem";
			this.filterToolStripMenuItem.Size = new System.Drawing.Size(202, 22);
			this.filterToolStripMenuItem.Text = "滤镜";
			// 
			// lOMOFilterToolStripMenuItem
			// 
			this.lOMOFilterToolStripMenuItem.Name = "lOMOFilterToolStripMenuItem";
			this.lOMOFilterToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
			this.lOMOFilterToolStripMenuItem.Text = "LOMO";
			this.lOMOFilterToolStripMenuItem.Click += new System.EventHandler(this.LOMOFilterToolStripMenuItemClick);
			// 
			// instagramToolStripMenuItem
			// 
			this.instagramToolStripMenuItem.Name = "instagramToolStripMenuItem";
			this.instagramToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
			this.instagramToolStripMenuItem.Text = "Instagram1977";
			this.instagramToolStripMenuItem.Click += new System.EventHandler(this.InstagramToolStripMenuItemClick);
			// 
			// nostalgiaToolStripMenuItem
			// 
			this.nostalgiaToolStripMenuItem.Name = "nostalgiaToolStripMenuItem";
			this.nostalgiaToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
			this.nostalgiaToolStripMenuItem.Text = "怀旧";
			this.nostalgiaToolStripMenuItem.Click += new System.EventHandler(this.NostalgiaToolStripMenuItemClick);
			// 
			// blackAndWhiteToolStripMenuItem
			// 
			this.blackAndWhiteToolStripMenuItem.Name = "blackAndWhiteToolStripMenuItem";
			this.blackAndWhiteToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
			this.blackAndWhiteToolStripMenuItem.Text = "黑白";
			this.blackAndWhiteToolStripMenuItem.Click += new System.EventHandler(this.BlackAndWhiteToolStripMenuItemClick);
			// 
			// shadowToolStripMenuItem
			// 
			this.shadowToolStripMenuItem.Name = "shadowToolStripMenuItem";
			this.shadowToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
			this.shadowToolStripMenuItem.Text = "暗调";
			this.shadowToolStripMenuItem.Click += new System.EventHandler(this.ShadowToolStripMenuItemClick);
			// 
			// cartoonToolStripMenuItem
			// 
			this.cartoonToolStripMenuItem.Name = "cartoonToolStripMenuItem";
			this.cartoonToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
			this.cartoonToolStripMenuItem.Text = "卡通";
			this.cartoonToolStripMenuItem.Click += new System.EventHandler(this.CartoonToolStripMenuItemClick);
			// 
			// oilpaintToolStripMenuItem
			// 
			this.oilpaintToolStripMenuItem.Name = "oilpaintToolStripMenuItem";
			this.oilpaintToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
			this.oilpaintToolStripMenuItem.Text = "油画";
			this.oilpaintToolStripMenuItem.Click += new System.EventHandler(this.OilpaintToolStripMenuItemClick);
			// 
			// susieToolStripMenuItem
			// 
			this.susieToolStripMenuItem.Name = "susieToolStripMenuItem";
			this.susieToolStripMenuItem.Size = new System.Drawing.Size(202, 22);
			this.susieToolStripMenuItem.Text = "使用Susie图片插件打开";
			this.susieToolStripMenuItem.Click += new System.EventHandler(this.SusieToolStripMenuItemClick);
			// 
			// susieArchiveToolStripMenuItem
			// 
			this.susieArchiveToolStripMenuItem.Name = "susieArchiveToolStripMenuItem";
			this.susieArchiveToolStripMenuItem.Size = new System.Drawing.Size(202, 22);
			this.susieArchiveToolStripMenuItem.Text = "使用Susie压缩包插件";
			this.susieArchiveToolStripMenuItem.Click += new System.EventHandler(this.SusieArchiveToolStripMenuItemClick);
			// 
			// firstimageToolStripMenuItem
			// 
			this.firstimageToolStripMenuItem.Name = "firstimageToolStripMenuItem";
			this.firstimageToolStripMenuItem.Size = new System.Drawing.Size(202, 22);
			this.firstimageToolStripMenuItem.Text = "返回第一张";
			this.firstimageToolStripMenuItem.Click += new System.EventHandler(this.FirstimageToolStripMenuItemClick);
			// 
			// slideplayerToolStripMenuItem
			// 
			this.slideplayerToolStripMenuItem.Name = "slideplayerToolStripMenuItem";
			this.slideplayerToolStripMenuItem.Size = new System.Drawing.Size(202, 22);
			this.slideplayerToolStripMenuItem.Text = "幻灯播放";
			this.slideplayerToolStripMenuItem.Click += new System.EventHandler(this.SlideplayerToolStripMenuItemClick);
			// 
			// toolStripSeparator2
			// 
			this.toolStripSeparator2.Name = "toolStripSeparator2";
			this.toolStripSeparator2.Size = new System.Drawing.Size(199, 6);
			// 
			// cWRotateToolStripMenuItem
			// 
			this.cWRotateToolStripMenuItem.Name = "cWRotateToolStripMenuItem";
			this.cWRotateToolStripMenuItem.Size = new System.Drawing.Size(202, 22);
			this.cWRotateToolStripMenuItem.Text = "顺时针旋转90°";
			this.cWRotateToolStripMenuItem.Click += new System.EventHandler(this.CWRotateToolStripMenuItemClick);
			// 
			// cCWRotateToolStripMenuItem
			// 
			this.cCWRotateToolStripMenuItem.Name = "cCWRotateToolStripMenuItem";
			this.cCWRotateToolStripMenuItem.Size = new System.Drawing.Size(202, 22);
			this.cCWRotateToolStripMenuItem.Text = "逆时针旋转90°";
			this.cCWRotateToolStripMenuItem.Click += new System.EventHandler(this.CCWRotateToolStripMenuItemClick);
			// 
			// horizontalFlipToolStripMenuItem
			// 
			this.horizontalFlipToolStripMenuItem.Name = "horizontalFlipToolStripMenuItem";
			this.horizontalFlipToolStripMenuItem.Size = new System.Drawing.Size(202, 22);
			this.horizontalFlipToolStripMenuItem.Text = "水平翻转";
			this.horizontalFlipToolStripMenuItem.Click += new System.EventHandler(this.HorizontalFlipToolStripMenuItemClick);
			// 
			// verticalFlipToolStripMenuItem
			// 
			this.verticalFlipToolStripMenuItem.Name = "verticalFlipToolStripMenuItem";
			this.verticalFlipToolStripMenuItem.Size = new System.Drawing.Size(202, 22);
			this.verticalFlipToolStripMenuItem.Text = "垂直翻转";
			this.verticalFlipToolStripMenuItem.Click += new System.EventHandler(this.VerticalFlipToolStripMenuItemClick);
			// 
			// opacityToolStripMenuItem
			// 
			this.opacityToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
			this.toolStripMenuItem3,
			this.toolStripMenuItem2,
			this.toolStripMenuItem4,
			this.toolStripMenuItem5});
			this.opacityToolStripMenuItem.Name = "opacityToolStripMenuItem";
			this.opacityToolStripMenuItem.Size = new System.Drawing.Size(202, 22);
			this.opacityToolStripMenuItem.Text = "透明度";
			// 
			// toolStripMenuItem3
			// 
			this.toolStripMenuItem3.Name = "toolStripMenuItem3";
			this.toolStripMenuItem3.Size = new System.Drawing.Size(108, 22);
			this.toolStripMenuItem3.Text = "100%";
			this.toolStripMenuItem3.Click += new System.EventHandler(this.ToolStripMenuItem3Click);
			// 
			// toolStripMenuItem2
			// 
			this.toolStripMenuItem2.Name = "toolStripMenuItem2";
			this.toolStripMenuItem2.Size = new System.Drawing.Size(108, 22);
			this.toolStripMenuItem2.Text = "80%";
			this.toolStripMenuItem2.Click += new System.EventHandler(this.ToolStripMenuItem2Click);
			// 
			// toolStripMenuItem4
			// 
			this.toolStripMenuItem4.Name = "toolStripMenuItem4";
			this.toolStripMenuItem4.Size = new System.Drawing.Size(108, 22);
			this.toolStripMenuItem4.Text = "60%";
			this.toolStripMenuItem4.Click += new System.EventHandler(this.ToolStripMenuItem4Click);
			// 
			// toolStripMenuItem5
			// 
			this.toolStripMenuItem5.Name = "toolStripMenuItem5";
			this.toolStripMenuItem5.Size = new System.Drawing.Size(108, 22);
			this.toolStripMenuItem5.Text = "40%";
			this.toolStripMenuItem5.Click += new System.EventHandler(this.ToolStripMenuItem5Click);
			// 
			// fullscreenToolStripMenuItem
			// 
			this.fullscreenToolStripMenuItem.Name = "fullscreenToolStripMenuItem";
			this.fullscreenToolStripMenuItem.Size = new System.Drawing.Size(202, 22);
			this.fullscreenToolStripMenuItem.Text = "全屏";
			this.fullscreenToolStripMenuItem.Click += new System.EventHandler(this.FullscreenToolStripMenuItemClick);
			// 
			// toolStripSeparator1
			// 
			this.toolStripSeparator1.Name = "toolStripSeparator1";
			this.toolStripSeparator1.Size = new System.Drawing.Size(199, 6);
			// 
			// saveimageToolStripMenuItem
			// 
			this.saveimageToolStripMenuItem.Name = "saveimageToolStripMenuItem";
			this.saveimageToolStripMenuItem.Size = new System.Drawing.Size(202, 22);
			this.saveimageToolStripMenuItem.Text = "保存图片";
			this.saveimageToolStripMenuItem.Click += new System.EventHandler(this.SaveimageToolStripMenuItemClick);
			// 
			// miniToolStripMenuItem
			// 
			this.miniToolStripMenuItem.Name = "miniToolStripMenuItem";
			this.miniToolStripMenuItem.Size = new System.Drawing.Size(202, 22);
			this.miniToolStripMenuItem.Text = "缩小到托盘";
			this.miniToolStripMenuItem.Click += new System.EventHandler(this.MiniToolStripMenuItemClick);
			// 
			// exitToolStripMenuItem
			// 
			this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
			this.exitToolStripMenuItem.Size = new System.Drawing.Size(202, 22);
			this.exitToolStripMenuItem.Text = "退出";
			this.exitToolStripMenuItem.Click += new System.EventHandler(this.ExitToolStripMenuItemClick);
			// 
			// notifyIcon1
			// 
			this.notifyIcon1.ContextMenuStrip = this.contextMenuStrip2;
			this.notifyIcon1.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon1.Icon")));
			this.notifyIcon1.Text = "MikuMikuViewer";
			this.notifyIcon1.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.NotifyIcon1MouseDoubleClick);
			// 
			// contextMenuStrip2
			// 
			this.contextMenuStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
			this.aboutToolStripMenuItem,
			this.exitToolStripMenuItem1});
			this.contextMenuStrip2.Name = "contextMenuStrip2";
			this.contextMenuStrip2.Size = new System.Drawing.Size(101, 48);
			// 
			// aboutToolStripMenuItem
			// 
			this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
			this.aboutToolStripMenuItem.Size = new System.Drawing.Size(100, 22);
			this.aboutToolStripMenuItem.Text = "关于";
			this.aboutToolStripMenuItem.Click += new System.EventHandler(this.AboutToolStripMenuItemClick);
			// 
			// exitToolStripMenuItem1
			// 
			this.exitToolStripMenuItem1.Name = "exitToolStripMenuItem1";
			this.exitToolStripMenuItem1.Size = new System.Drawing.Size(100, 22);
			this.exitToolStripMenuItem1.Text = "退出";
			this.exitToolStripMenuItem1.Click += new System.EventHandler(this.ExitToolStripMenuItem1Click);
			// 
			// timer1
			// 
			this.timer1.Interval = 3000;
			this.timer1.Tick += new System.EventHandler(this.Timer1Tick);
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(261, 261);
			this.Controls.Add(this.pictureBox1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "MainForm";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "MikuMikuViewer";
			this.TopMost = true;
			this.SizeChanged += new System.EventHandler(this.MainFormSizeChanged);
			this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MainFormKeyDown);
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
			this.contextMenuStrip1.ResumeLayout(false);
			this.contextMenuStrip2.ResumeLayout(false);
			this.ResumeLayout(false);

		}
	}
}
