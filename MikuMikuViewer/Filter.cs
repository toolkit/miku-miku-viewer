﻿/*
 * 由SharpDevelop创建。
 * 用户： Administrator
 * 日期: 2022/1/6/周四
 * 时间: 11:32
 * 
 * 要改变这种模板请点击 工具|选项|代码编写|编辑标准头文件
 */
using System;
using System.Drawing;
using System.Drawing.Imaging;

namespace MikuMikuViewer
{
	/// <summary>
	/// Description of Filter.
	/// </summary>
	public class Filter
	{
		/// <summary>
		/// 黑白连环画滤镜
		/// </summary>
		/// <param name="a"></param>
		/// <returns></returns>
		public Bitmap BlackAndWhiteFilter(Bitmap a)
        {
            Bitmap srcBitmap = new Bitmap(a);
            int w = srcBitmap.Width;
            int h = srcBitmap.Height;
            System.Drawing.Imaging.BitmapData srcData = srcBitmap.LockBits(new Rectangle(0, 0, w, h), System.Drawing.Imaging.ImageLockMode.ReadWrite, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
            unsafe{
            byte* pSrc = (byte*)srcData.Scan0;
            int offset = srcData.Stride - w * 4;
            int red = 0, green = 0, blue = 0, gray = 0;
            for (int j = 0; j < h; j++)
            {
                for (int i = 0; i < w; i++)
                {
                    blue = (Math.Abs(pSrc[0] - pSrc[1] + pSrc[0] + pSrc[2]) * pSrc[1]) >> 8;
                    green = (Math.Abs(pSrc[0] - pSrc[1] + pSrc[0] + pSrc[2]) * pSrc[2]) >> 8;
                    red = (Math.Abs(pSrc[1] - pSrc[0] + pSrc[1] + pSrc[2]) * pSrc[2]) >> 8;
                    blue = Math.Max(0, Math.Min(blue, 255));
                    green = Math.Max(0, Math.Min(green, 255));
                    red = Math.Max(0, Math.Min(red, 255));
                    gray = (blue + green + red) / 3;
                    red = Math.Min(255, gray + 10);
                    pSrc[0] = (byte)gray;
                    pSrc[1] = (byte)red;
                    pSrc[2] = (byte)red;
                    pSrc += 4;
                }
                pSrc += offset;
            }}
            srcBitmap.UnlockBits(srcData);
            return srcBitmap;
        }
		
		/// <summary>
		/// 暗调滤镜
		/// </summary>
		/// <param name="a"></param>
		/// <returns></returns>
		public Bitmap ShadowFilter(Bitmap a)
        {
            Bitmap srcBitmap = new Bitmap(a);
            int w = srcBitmap.Width;
            int h = srcBitmap.Height;
            System.Drawing.Imaging.BitmapData srcData = srcBitmap.LockBits(new Rectangle(0, 0, w, h), System.Drawing.Imaging.ImageLockMode.ReadWrite, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
            unsafe{
            byte* pSrc = (byte*)srcData.Scan0;
            int offset = srcData.Stride - w * 4;
            int r, g, b;
            for (int j = 0; j < h; j++)
            {
                for (int i = 0; i < w; i++)
                {
                    b = pSrc[0];
                    g = pSrc[1];
                    r = pSrc[2];
                    pSrc[2] = (byte)(r * r / 255);
                    pSrc[1] = (byte)(g * g / 255);
                    pSrc[0] = (byte)(b * b / 255);
                    pSrc += 4;
                }
                pSrc += offset;
            }}
            srcBitmap.UnlockBits(srcData);
            return srcBitmap;
        }
		
		/// <summary>
		/// 怀旧风格滤镜
		/// </summary>
		/// <param name="a"></param>
		/// <returns></returns>
		public Bitmap NostalgiaFilter(Bitmap a)
        {
            Bitmap srcBitmap = new Bitmap(a);
            int w = srcBitmap.Width;
            int h = srcBitmap.Height;
            System.Drawing.Imaging.BitmapData srcData = srcBitmap.LockBits(new Rectangle(0, 0, w, h), System.Drawing.Imaging.ImageLockMode.ReadWrite, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
            unsafe{
            byte* pSrc = (byte*)srcData.Scan0;
            int offset = srcData.Stride - w * 4;
            int b = 0, g = 0, r = 0, gray = 0;
            for (int j = 0; j < h; j++)
            {
                for (int i = 0; i < w; i++)
                {
                    b = pSrc[0];
                    g = pSrc[1];
                    r = pSrc[2];
                    gray = (272 * r + 534 * g + 131 * b) / 1000;
                    pSrc[0] = (byte)(Math.Min(255, Math.Max(0, gray)));
                    gray = (349 * r + 686 * g + 168 * b) / 1000;
                    pSrc[1] = (byte)(Math.Min(255, Math.Max(0, gray)));
                    gray = (393 * r + 769 * g + 189 * b) / 1000;
                    pSrc[2] = (byte)(Math.Min(255, Math.Max(0, gray)));
                    pSrc += 4;
                }
                pSrc += offset;
            }}
            srcBitmap.UnlockBits(srcData);
            return srcBitmap;
        }
		
		/// <summary>
		/// 油画滤镜
		/// </summary>
		/// <param name="src"></param>
		/// <param name="intensity"></param>
		/// <returns></returns>
		public Bitmap OilpaintFilter(Bitmap src, int intensity) 
		{
			Bitmap srcBitmap = new Bitmap(src);
			int w = srcBitmap.Width;
			int h = srcBitmap.Height;
			System.Drawing.Imaging.BitmapData srcData = srcBitmap.LockBits(new Rectangle(0, 0, w, h), System.Drawing.Imaging.ImageLockMode.ReadWrite, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
			IntPtr ptr = srcData.Scan0;
			int bytes = h * srcData.Stride;
			byte[] srcValues = new byte[bytes];
			System.Runtime.InteropServices.Marshal.Copy(ptr, srcValues, 0, bytes);
			byte[] tempValues = (byte[])srcValues.Clone();
			int stride = srcData.Stride;
			Random ran = new Random();
			int k = 0;
			int dx = 0;
			int dy = 0;
			for (int j = 0; j < h; j++) 
			{
				for (int i = 0; i < w; i++) 
				{
					k = ran.Next(intensity);
					dx = (i + k) >= w ? w - 1 : (i + k);
					dy = (j + k) >= h ? h - 1 : (j + k);
					tempValues[i * 4 + j * w * 4] = (byte)srcValues[dx * 4 + dy * w * 4];
					tempValues[i * 4 + 1 + j * w * 4] = (byte)srcValues[dx * 4 + 1 + dy * w * 4];
					tempValues[i * 4 + 2 + j * w * 4] = (byte)srcValues[dx * 4 + 2 + dy * w * 4];
				}
			}
			srcValues = (byte[])tempValues.Clone();
			System.Runtime.InteropServices.Marshal.Copy(srcValues, 0, ptr, bytes);
			srcBitmap.UnlockBits(srcData);
			return srcBitmap;
		}
        
        /// <summary>
        /// 漫画滤镜
        /// </summary>
        /// <param name="src"></param>
        /// <param name="edgeIntensity"></param>
        /// <returns></returns>
        public Bitmap CartoonFilter(Bitmap src, int edgeIntensity)
        {
            Bitmap edgeBitmap = SobelEdgeDetect(src, edgeIntensity);   
            Bitmap dst = OilpaintFilterProcess(new Bitmap(src),12,10);;
            int w = dst.Width;
            int h = dst.Height;
            BitmapData dstData = dst.LockBits(new Rectangle(0, 0, w, h), ImageLockMode.ReadWrite, PixelFormat.Format32bppArgb);
            BitmapData edgeData = edgeBitmap.LockBits(new Rectangle(0, 0, w, h), ImageLockMode.ReadOnly, PixelFormat.Format32bppArgb);
            unsafe{
            byte* pEdge = (byte*)edgeData.Scan0;
            byte* pDst = (byte*)dstData.Scan0;
            int offset = dstData.Stride - w * 4;
            for (int j = 0; j < h; j++)
            {
                for (int i = 0; i < w; i++)
                {
                    pDst[0] = (byte)(pDst[0] * pEdge[0] / 255);
                    pDst[1] = (byte)(pDst[1] * pEdge[1] / 255);
                    pDst[2] = (byte)(pDst[2] * pEdge[2] / 255);
                    pDst[3] = (byte)255;
                    pEdge += 4;
                    pDst += 4;
                }
                pEdge += offset;
                pDst += offset;
            }}
            dst.UnlockBits(dstData);
            edgeBitmap.UnlockBits(edgeData);
            return dst;
        }
		
		/// <summary>
		/// LOMO滤镜
		/// </summary>
		/// <param name="src"></param>
		/// <returns></returns>
		public Bitmap LOMOFilter(Bitmap src)
        {
            Bitmap srcBitmap = new Bitmap(src);
            Bitmap dst = new Bitmap(src);
            int w = dst.Width;
            int h = dst.Height;
            BitmapData dstData = dst.LockBits(new Rectangle(0, 0, w, h), ImageLockMode.ReadWrite, PixelFormat.Format32bppArgb);
            BitmapData srcData = srcBitmap.LockBits(new Rectangle(0, 0, w, h), ImageLockMode.ReadOnly, PixelFormat.Format32bppArgb);
            unsafe{
            byte* pSrc = (byte*)srcData.Scan0;
            byte* pDst = (byte*)dstData.Scan0;
            int offset = dstData.Stride - w * 4;
            int r,g,b;
            for (int j = 0; j < h; j++)
            {
                for (int i = 0; i < w; i++)
                {
                    b = ModeSmoothLight(pSrc[0], pDst[0]);
                    g = ModeSmoothLight(pSrc[1], pDst[1]);
                    r = ModeSmoothLight(pSrc[2], pDst[2]);
                    b = ModeExclude(b, 80);
                    g = ModeExclude(g, 15);
                    r = ModeExclude(r, 5);
                    pDst[0] = (byte)b;
                    pDst[1] = (byte)g;
                    pDst[2] = (byte)r;
                    pDst[3] = (byte)255;
                    pSrc += 4;
                    pDst += 4;
                }
                pSrc += offset;
                pDst += offset;
            }}
            dst.UnlockBits(dstData);
            srcBitmap.UnlockBits(srcData);
            return dst;
        }
		

		
		/// <summary>
		/// Instagram1977滤镜
		/// </summary>
		/// <param name="a"></param>
		/// <returns></returns>
		public  Bitmap Instagram1977(Bitmap a)
        {
            Bitmap srcBitmap = new Bitmap(a);
            int w = srcBitmap.Width;
            int h = srcBitmap.Height;
            int[] processAB = new int[256] { 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 65, 66, 68, 69, 70, 71, 72, 73, 75, 76, 77, 78, 79, 80, 82, 83, 84, 85, 86, 87, 89, 90, 91, 92, 93, 95, 96, 97, 98, 99, 100, 102, 103, 104, 105, 106, 107, 109, 110, 111, 112, 113, 115, 116, 117, 118, 119, 120, 122, 123, 124, 125, 126, 127, 129, 130, 131, 132, 133, 134, 136, 137, 138, 139, 139, 140, 142, 143, 144, 145, 146, 147, 149, 150, 151, 152, 153, 154, 156, 157, 158, 159, 160, 161, 163, 164, 165, 166, 167, 169, 170, 171, 172, 173, 174, 176, 177, 178, 179, 180, 181, 183, 184, 185, 186, 187, 189, 190, 191, 192, 193, 194, 196, 197, 198, 199, 200, 201, 203, 204, 205, 206, 207, 208, 210, 211, 212, 212, 212, 212, 212, 212, 212, 212, 212, 212, 212, 212, 212, 212, 212, 212, 212, 212, 212, 212, 212, 212, 212, 212, 212, 212, 212, 212, 212, 212, 212, 212, 212, 212, 212, 212, 212, 212, 212, 212, 212, 212, 212, 212, 212, 212, 212, 212, 212, 212, 212, 212, 212, 212, 212, 212, 212, 212, 212, 212, 212, 212, 212, 212, 212 };//
            int[] processAG = new int[256] { 57, 57, 57, 57, 57, 57, 57, 57, 57, 57, 57, 57, 57, 57, 57, 57, 57, 57, 57, 57, 57, 57, 57, 57, 57, 57, 57, 57, 57, 57, 57, 57, 57, 57, 57, 57, 57, 57, 57, 57, 57, 57, 57, 57, 57, 57, 57, 57, 57, 57, 57, 57, 57, 57, 57, 57, 57, 57, 57, 57, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 73, 73, 74, 75, 76, 77, 78, 79, 80, 81, 83, 84, 85, 85, 86, 87, 88, 89, 90, 91, 93, 94, 95, 96, 96, 97, 98, 99, 100, 102, 103, 104, 105, 106, 107, 108, 108, 109, 110, 112, 113, 114, 115, 116, 117, 118, 119, 120, 120, 122, 123, 124, 125, 126, 126, 127, 128, 129, 130, 131, 132, 133, 134, 135, 136, 137, 138, 139, 140, 142, 143, 143, 144, 145, 146, 147, 148, 149, 150, 152, 153, 154, 155, 155, 156, 157, 158, 159, 160, 162, 163, 164, 165, 166, 167, 167, 168, 169, 170, 172, 173, 174, 175, 176, 177, 178, 179, 179, 181, 182, 183, 184, 185, 186, 187, 188, 189, 190, 191, 192, 193, 194, 195, 196, 197, 198, 199, 200, 202, 202, 203, 204, 205, 206, 207, 208, 209, 210, 212, 213, 214, 214, 215, 216, 217, 218, 219, 221, 222, 223, 224, 225, 226, 226, 227, 228, 229, 231, 232, 233, 234, 235, 236, 237, 237, 238, 239, 241, 242, 243, 244, 245, 246, 247, 248, 249, 249, 251, 252, 253, 254 };//
            int[] processAR = new int[256] { 81, 81, 81, 81, 81, 81, 81, 81, 81, 81, 81, 81, 81, 81, 81, 81, 81, 81, 81, 81, 81, 81, 81, 81, 81, 81, 81, 81, 81, 81, 81, 81, 81, 81, 81, 81, 81, 81, 81, 81, 81, 81, 81, 81, 81, 81, 81, 81, 81, 81, 81, 82, 84, 84, 85, 86, 88, 88, 89, 91, 92, 93, 93, 95, 96, 97, 98, 99, 100, 101, 103, 103, 104, 106, 107, 107, 108, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 121, 121, 122, 123, 125, 126, 126, 128, 129, 130, 131, 132, 133, 134, 135, 136, 137, 138, 140, 140, 141, 143, 144, 145, 145, 147, 148, 149, 150, 151, 152, 154, 154, 155, 156, 158, 159, 159, 161, 162, 163, 164, 164, 165, 166, 167, 168, 169, 170, 171, 173, 173, 174, 176, 177, 178, 178, 180, 181, 182, 183, 184, 185, 186, 187, 188, 189, 191, 192, 192, 193, 195, 196, 196, 198, 199, 200, 201, 202, 203, 204, 206, 206, 207, 208, 210, 210, 211, 213, 214, 215, 215, 217, 218, 219, 220, 221, 222, 223, 225, 225, 225, 225, 225, 225, 225, 225, 225, 225, 225, 225, 225, 225, 225, 225, 225, 225, 225, 225, 225, 225, 225, 225, 225, 225, 225, 225, 225, 225, 225, 225, 225, 225, 225, 225, 225, 225, 225, 225, 225, 225, 225, 225, 225, 225, 225, 225, 225, 225, 225, 225, 225, 225, 225, 225, 225, 225, 225, 225, 225, 225, 225, 225, 225, 225, 225, 225, 225, 225 };//
            System.Drawing.Imaging.BitmapData srcData = srcBitmap.LockBits(new Rectangle(0, 0, w, h), System.Drawing.Imaging.ImageLockMode.ReadWrite, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
            unsafe{
            byte* pSrc = (byte*)srcData.Scan0;
            int offset = srcData.Stride - w * 4;
            for (int j = 0; j < h; j++)
            {
                for (int i = 0; i < w; i++)
                {
                    pSrc[0] = (byte)processAB[pSrc[0]];
                    pSrc[1] = (byte)processAG[pSrc[1]];
                    pSrc[2] = (byte)processAR[pSrc[2]];
                    pSrc += 4;
                }
                pSrc += offset;
            }
            }
            srcBitmap.UnlockBits(srcData);
            return srcBitmap;
        }
		//******************
		private Bitmap SobelEdgeDetect(Bitmap src,int edgeIntensity)
        {
            int w = src.Width;
            int h = src.Height;
            Bitmap a = new Bitmap(w, h, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
            BitmapData srcData = src.LockBits(new Rectangle(0, 0, w, h), ImageLockMode.ReadOnly, PixelFormat.Format32bppArgb);
            BitmapData dstData = a.LockBits(new Rectangle(0, 0, w, h), ImageLockMode.WriteOnly, PixelFormat.Format32bppArgb);
            unsafe{
            byte* pIn = (byte*)srcData.Scan0.ToPointer();
            byte* pOut = (byte*)dstData.Scan0.ToPointer();
            byte* p;
            int offset = srcData.Stride - w * 4;
            int r0, r1, r2, r3, r4, r5, r6, r7, r8;
            int g1, g2, g3, g4, g5, g6, g7, g8, g0;
            int b1, b2, b3, b4, b5, b6, b7, b8, b0;
            double vR, vG, vB;
            int stride = srcData.Stride;
            int []lightMap={0, 1, 2, 3, 5, 6, 7, 8, 9, 10, 11, 12, 14, 15, 16, 17, 18, 19, 20, 22, 23, 24, 25, 26, 27, 28, 29, 31, 32, 33, 34, 35, 36, 37, 39, 40, 41, 42, 43, 44, 45, 47, 48, 49, 50, 51, 52, 53, 54, 56, 57, 58, 59, 60, 61, 62, 64, 65, 66, 67, 68, 69, 70, 71, 73, 74, 75, 76, 77, 78, 79, 81, 82, 83, 84, 85, 86, 87, 88, 90, 91, 92, 93, 94, 95, 96, 98, 99, 100, 101, 102, 103, 104, 105, 107, 108, 109, 110, 111, 112, 113, 115, 116, 117, 118, 119, 120, 121, 123, 124, 125, 126, 127, 128, 129, 130, 132, 133, 134, 135, 136, 137, 138, 140, 141, 142, 143, 144, 145, 146, 147, 149, 150, 151, 152, 153, 154, 155, 156, 158, 159, 160, 161, 162, 163, 164, 165, 166, 168, 169, 170, 171, 172, 173, 174, 175, 176, 177, 178, 179, 181, 182, 183, 184, 185, 186, 187, 188, 189, 190, 191, 192, 193, 194, 195, 196, 197, 198, 199, 200, 201, 202, 203, 204, 205, 206, 207, 208, 209, 210, 211, 212, 212, 213, 214, 215, 216, 217, 218, 219, 220, 220, 221, 222, 223, 224, 225, 225, 226, 227, 228, 229, 229, 230, 231, 232, 233, 233, 234, 235, 235, 236, 237, 238, 238, 239, 240, 240, 241, 242, 242, 243, 243, 244, 245, 245, 246, 246, 247, 248, 248, 249, 249, 250, 250, 251, 251, 252, 252, 253, 253, 253, 254, 254, 255, 255};
            int gray = 0;
            for (int j = 0; j < h; j++)
            {
                for (int i = 0; i < w; i++)
                {
                    if (i == 0 || i == w - 1 || j == 0 || j == h - 1)
                    {
                        pOut[0] = (byte)0;
                        pOut[1] = (byte)0;
                        pOut[2] = (byte)0;
                        pOut[3] = (byte)255;
                    }
                    else
                    {
                        //左上
                        p = pIn - stride - 4;
                        r1 = p[2];
                        g1 = p[1];
                        b1 = p[0];
                        //正上
                        p = pIn - stride;
                        r2 = p[2];
                        g2 = p[1];
                        b2 = p[0];
                        //右上
                        p = pIn - stride + 4;
                        r3 = p[2];
                        g3 = p[1];
                        b3 = p[0];
                        //左
                        p = pIn - 4;
                        r4 = p[2];
                        g4 = p[1];
                        b4 = p[0];
                        //右
                        p = pIn + 4;
                        r5 = p[2];
                        g5 = p[1];
                        b5 = p[0];
                        //左下
                        p = pIn + stride - 4;
                        r6 = p[2];
                        g6 = p[1];
                        b6 = p[0];
                        //正下
                        p = pIn + stride;
                        r7 = p[2];
                        g7 = p[1];
                        b7 = p[0];
                        // 右下 
                        p = pIn + stride + 4;
                        r8 = p[2];
                        g8 = p[1];
                        b8 = p[0];
                        //中心点
                        p = pIn;
                        r0 = p[2];
                        g0 = p[1];
                        b0 = p[0];
                        //使用模板
                        vR = (double)(Math.Abs(r1 + 2 * r4 + r6 - r3 - 2 * r5 - r8) + Math.Abs(r1 + 2 * r2 + r3 - r6 - 2 * r7 - r8));
                        vG = (double)(Math.Abs(g1 + 2 * g4 + g6 - g3 - 2 * g5 - g8) + Math.Abs(g1 + 2 * g2 + g3 - g6 - 2 * g7 - g8));
                        vB = (double)(Math.Abs(b1 + 2 * b4 + b6 - b3 - 2 * b5 - b8) + Math.Abs(b1 + 2 * b2 + b3 - b6 - 2 * b7 - b8));
                        vB = Math.Min(255, Math.Max(0, vB));
                        vG = Math.Min(255, Math.Max(0, vG));
                        vR = Math.Min(255, Math.Max(0, vR));
                        gray = lightMap[255 - (int)(vB + vG + vR) / 3];
                        gray = Math.Min(255, gray + edgeIntensity);
                        pOut[0] = (byte)gray;
                        pOut[1] = (byte)gray;
                        pOut[2] = (byte)gray;
                        pOut[3] = (byte)255;
                    }
                    pIn += 4;
                    pOut += 4;
                }
                pIn += offset;
                pOut += offset;
            }}
            a.UnlockBits(dstData);
            src.UnlockBits(srcData);
            return a;
        }
		
		/// <summary>
		/// 水彩滤镜
		/// </summary>
		/// <param name="srcBitmap"></param>
		/// <param name="radius"></param>
		/// <param name="smooth"></param>
		/// <returns></returns>
        public Bitmap OilpaintFilterProcess(Bitmap srcBitmap, int radius, int smooth)
        {
            if (radius == 0)
                return srcBitmap;
            smooth = smooth < 1 ? 1 : smooth;
            smooth = Math.Max(1, smooth);
            Bitmap a = new Bitmap(srcBitmap);
            int w = srcBitmap.Width;
            int h = srcBitmap.Height;
            if (radius > Math.Min(w, h) / 2)
                radius = (int)(Math.Min(w, h) / 2 - 0.5);
            System.Drawing.Imaging.BitmapData srcData = a.LockBits(new Rectangle(0, 0, w, h), System.Drawing.Imaging.ImageLockMode.ReadWrite, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
            IntPtr ptr = srcData.Scan0;
            int bytes = h * srcData.Stride;
            byte[] srcValues = new byte[bytes];
            System.Runtime.InteropServices.Marshal.Copy(ptr, srcValues, 0, bytes);
            byte[] tempValues = (byte[])srcValues.Clone();
            int stride = srcData.Stride;
            int i, j, k;
            int unit = 4;
            int[] gray_bt = new int[smooth];
            int[] r_bt = new int[smooth];
            int[] g_bt = new int[smooth];
            int[] b_bt = new int[smooth];
            int[] gray_bt_src = new int[smooth];
            int[] r_bt_src = new int[smooth];
            int[] g_bt_src = new int[smooth];
            int[] b_bt_src = new int[smooth];
            int r, g, b;
            int gray = 0, bt_index = 0, max = 0, maxindex = 0;
            i = 0;
            bool frist = true;
            int pos = 0;
            for (j = 0; j < h; j++)
            {
                if (frist)
                {
                    for (int m = -radius; m <= radius; m++)
                    {
                        for (int n = -radius; n <= radius; n++)
                        {
                            pos = Math.Abs(n) * unit + Math.Abs(m) * stride;
                            b = srcValues[pos++];
                            g = srcValues[pos++];
                            r = srcValues[pos];
                            gray = (b + g + r) / 3;
                            bt_index = gray * smooth >> 8;
                            gray_bt_src[bt_index]++;
                            b_bt_src[bt_index] += b;
                            g_bt_src[bt_index] += g;
                            r_bt_src[bt_index] += r;
                        }
                    }
                    Array.Copy(gray_bt_src, gray_bt, smooth);
                    Array.Copy(b_bt_src, b_bt, smooth);
                    Array.Copy(g_bt_src, g_bt, smooth);
                    Array.Copy(r_bt_src, r_bt, smooth);
                    max = 0;
                    maxindex = 0;
                    for (k = 0; k < smooth; k++)
                    {
                        if (max < gray_bt[k])
                        {
                            max = gray_bt[k];
                            maxindex = k;
                        }
                    }
                    pos = j * stride;
                    tempValues[pos++] = (byte)(b_bt[maxindex] / max);
                    tempValues[pos++] = (byte)(g_bt[maxindex] / max);
                    tempValues[pos] = (byte)(r_bt[maxindex] / max);
                    frist = false;
                }
                else
                {
                    for (int m = -radius; m <= radius; m++)
                    {
                        pos = Math.Abs(m) * unit + Math.Abs(j - radius - 1) * stride;
                        b = srcValues[pos++];
                        g = srcValues[pos++];
                        r = srcValues[pos];
                        gray = (b + g + r) / 3;
                        bt_index = gray * smooth >> 8;
                        gray_bt_src[bt_index]--;
                        b_bt_src[bt_index] -= b;
                        g_bt_src[bt_index] -= g;
                        r_bt_src[bt_index] -= r;

                        pos = Math.Abs(m) * unit + Math.Abs(j + radius) % h * stride;
                        b = srcValues[pos++];
                        g = srcValues[pos++];
                        r = srcValues[pos];
                        gray = (b + g + r) / 3;
                        bt_index = gray * smooth >> 8;
                        gray_bt_src[bt_index]++;
                        b_bt_src[bt_index] += b;
                        g_bt_src[bt_index] += g;
                        r_bt_src[bt_index] += r;
                    }
                    Array.Copy(gray_bt_src, gray_bt, smooth);
                    Array.Copy(b_bt_src, b_bt, smooth);
                    Array.Copy(g_bt_src, g_bt, smooth);
                    Array.Copy(r_bt_src, r_bt, smooth);
                }
                for (i = 1; i < w; i++)
                {
                    for (int m = -radius; m <= radius; m++)
                    {
                        pos = Math.Abs(i - radius - 1) * unit + Math.Abs(j + m) % h * stride;
                        b = srcValues[pos++];
                        g = srcValues[pos++];
                        r = srcValues[pos];
                        gray = (b + g + r) / 3;
                        bt_index = gray * smooth >> 8;
                        gray_bt[bt_index]--;
                        b_bt[bt_index] -= b;
                        g_bt[bt_index] -= g;
                        r_bt[bt_index] -= r;

                        pos = Math.Abs(i + radius) % w * unit + Math.Abs(j + m) % h * stride;
                        b = srcValues[pos++];
                        g = srcValues[pos++];
                        r = srcValues[pos];
                        gray = (b + g + r) / 3;
                        bt_index = gray * smooth >> 8;
                        gray_bt[bt_index]++;
                        b_bt[bt_index] += b;
                        g_bt[bt_index] += g;
                        r_bt[bt_index] += r;
                    }
                    max = 0;
                    maxindex = 0;
                    for (k = 0; k < smooth; k++)
                    {
                        if (max < gray_bt[k])
                        {
                            max = gray_bt[k];
                            maxindex = k;
                        }
                    }
                    pos = i * unit + j * stride;
                    tempValues[pos++] = (byte)(b_bt[maxindex] / max);
                    tempValues[pos++] = (byte)(g_bt[maxindex] / max);
                    tempValues[pos] = (byte)(r_bt[maxindex] / max);
                }             
            }
            srcValues = (byte[])tempValues.Clone();
            System.Runtime.InteropServices.Marshal.Copy(srcValues, 0, ptr, bytes);
            a.UnlockBits(srcData);
            return a;
        }
        
        		private int ModeSmoothLight(int basePixel, int mixPixel)
        {
            int res = 0;
            res = mixPixel > 128 ? ((int)((float)basePixel + ((float)mixPixel + (float)mixPixel - 255.0f) * ((Math.Sqrt((float)basePixel / 255.0f)) * 255.0f - (float)basePixel) / 255.0f)) :
                  ((int)((float)basePixel + ((float)mixPixel + (float)mixPixel - 255.0f) * ((float)basePixel - (float)basePixel * (float)basePixel / 255.0f) / 255.0f));
            return Math.Min(255, Math.Max(0, res)); 
        }
		
		private int ModeExclude(int basePixel, int mixPixel)
        {
            int res = 0;
            res = (mixPixel + basePixel) - mixPixel * basePixel / 128;
            return Math.Min(255, Math.Max(0, res));
        }
		//*******************
	}
}
