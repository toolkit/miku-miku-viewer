﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Runtime.InteropServices;

namespace Zgke.MyImage.ImageFile
{
	// Token: 0x02000002 RID: 2
	public class ImagePsd
	{
		// Token: 0x06000001 RID: 1 RVA: 0x00002050 File Offset: 0x00001050
		public ImagePsd(string p_FileFullPath)
		{
			if (!File.Exists(p_FileFullPath))
			{
				return;
			}
			FileStream fileStream = File.Open(p_FileFullPath, FileMode.Open);
			byte[] array = new byte[26];
			fileStream.Read(array, 0, 26);
			this.m_Head = new ImagePsd.PSDHEAD(array);
			this.m_ColorModel = new ImagePsd.ColorModel(fileStream);
			long position = fileStream.Position;
			for (;;)
			{
				ImagePsd.BIM bim = new ImagePsd.BIM(fileStream);
				if (!bim.Read || fileStream.Position - position >= (long)((ulong)this.m_ColorModel.BIMSize))
				{
					break;
				}
				this.m_8BIMList.Add(bim);
			}
			this.m_LayerMaskInfo = new ImagePsd.LayerMaskInfo(fileStream);
			this.m_ImageData = new ImagePsd.ImageData(fileStream, this.m_Head);
			if (this.m_Head.ColorMode == 2)
			{
				this.m_ImageData.PSDImage.Palette = this.m_ColorModel.ColorData;
			}
			fileStream.Close();
		}

		// Token: 0x06000002 RID: 2 RVA: 0x00002131 File Offset: 0x00001131
		public ImagePsd()
		{
			this.NewPsd();
		}

		// Token: 0x06000003 RID: 3 RVA: 0x0000216A File Offset: 0x0000116A
		public void NewPsd()
		{
			this.m_Head = new ImagePsd.PSDHEAD(new byte[]
			{
				56,
				66,
				80,
				83,
				0,
				1,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				3,
				0,
				0,
				1,
				44,
				0,
				0,
				1,
				180,
				0,
				8,
				0,
				3
			});
			this.m_ColorModel = new ImagePsd.ColorModel();
			this.m_ImageData = new ImagePsd.ImageData();
			this.m_LayerMaskInfo = new ImagePsd.LayerMaskInfo();
		}

		// Token: 0x06000004 RID: 4 RVA: 0x000021F8 File Offset: 0x000011F8
		public void Save(string p_FileFullName)
		{
			if (this.PSDImage != null)
			{
				Image psdimage = this.PSDImage;
				this.NewPsd();
				this.PSDImage = (Bitmap)psdimage;
				int width = this.PSDImage.Width;
				int height = this.PSDImage.Height;
				this.m_Head.Height = (uint)height;
				this.m_Head.Width = (uint)width;
				byte[] array = new byte[]
				{
					56,
					66,
					73,
					77,
					3,
					237,
					0,
					0,
					0,
					0,
					0,
					16,
					0,
					150,
					0,
					0,
					0,
					1,
					0,
					1,
					0,
					150,
					0,
					0,
					0,
					1,
					0,
					5,
					56,
					66,
					73,
					77,
					3,
					243,
					0,
					0,
					0,
					0,
					0,
					8,
					0,
					0,
					0,
					0,
					0,
					0,
					0,
					0,
					56,
					66,
					73,
					77,
					39,
					16,
					0,
					0,
					0,
					0,
					0,
					10,
					0,
					1,
					0,
					0,
					0,
					0,
					0,
					0,
					0,
					2
				};
				this.m_ColorModel.BIMSize = (uint)array.Length;
				FileStream fileStream = new FileStream(p_FileFullName, FileMode.Create, FileAccess.Write);
				byte[] bytes = this.m_Head.GetBytes();
				fileStream.Write(bytes, 0, bytes.Length);
				bytes = this.m_ColorModel.GetBytes();
				fileStream.Write(bytes, 0, bytes.Length);
				fileStream.Write(array, 0, array.Length);
				fileStream.Write(new byte[2], 0, 2);
				bytes = this.m_LayerMaskInfo.GetBytes();
				fileStream.Write(bytes, 0, bytes.Length);
				Bitmap bitmap = new Bitmap(width, height, PixelFormat.Format24bppRgb);
				Graphics graphics = Graphics.FromImage(bitmap);
				graphics.DrawImage(this.PSDImage, 0, 0, width, height);
				graphics.Dispose();
				BitmapData bitmapData = bitmap.LockBits(new Rectangle(0, 0, width, height), ImageLockMode.ReadOnly, PixelFormat.Format24bppRgb);
				byte[] array2 = new byte[bitmapData.Stride * height];
				Marshal.Copy(bitmapData.Scan0, array2, 0, array2.Length);
				byte[] array3 = new byte[bitmap.Width * bitmap.Height * 3];
				int num = width * height;
				int num2 = num * 2;
				for (int num3 = 0; num3 != height; num3++)
				{
					int num4 = num3 * bitmapData.Stride;
					int num5 = num3 * width;
					for (int num6 = 0; num6 != width; num6++)
					{
						array3[num5 + num6] = array2[num4 + num6 * 3 + 2];
						array3[num5 + num + num6] = array2[num4 + num6 * 3 + 1];
						array3[num5 + num2 + num6] = array2[num4 + num6 * 3];
					}
				}
				bitmap.UnlockBits(bitmapData);
				bitmap.Dispose();
				fileStream.Write(array3, 0, array3.Length);
				fileStream.Close();
			}
		}

		// Token: 0x17000001 RID: 1
		// (get) Token: 0x06000005 RID: 5 RVA: 0x00002414 File Offset: 0x00001414
		// (set) Token: 0x06000006 RID: 6 RVA: 0x00002421 File Offset: 0x00001421
		public Bitmap PSDImage
		{
			get
			{
				return this.m_ImageData.PSDImage;
			}
			set
			{
				this.m_ImageData.PSDImage = value;
			}
		}

		// Token: 0x04000001 RID: 1
		private ImagePsd.PSDHEAD m_Head;

		// Token: 0x04000002 RID: 2
		private ImagePsd.ColorModel m_ColorModel;

		// Token: 0x04000003 RID: 3
		private IList<ImagePsd.BIM> m_8BIMList = new List<ImagePsd.BIM>();

		// Token: 0x04000004 RID: 4
		private ImagePsd.LayerMaskInfo m_LayerMaskInfo;

		// Token: 0x04000005 RID: 5
		private ImagePsd.ImageData m_ImageData;

		// Token: 0x02000003 RID: 3
		private class PSDHEAD
		{
			// Token: 0x06000007 RID: 7 RVA: 0x0000242F File Offset: 0x0000142F
			public byte[] GetBytes()
			{
				return this.m_HeadBytes;
			}

			// Token: 0x06000008 RID: 8 RVA: 0x00002437 File Offset: 0x00001437
			public PSDHEAD(byte[] p_Data)
			{
				this.m_HeadBytes = p_Data;
			}

			// Token: 0x17000002 RID: 2
			// (get) Token: 0x06000009 RID: 9 RVA: 0x00002453 File Offset: 0x00001453
			// (set) Token: 0x0600000A RID: 10 RVA: 0x0000245D File Offset: 0x0000145D
			public byte Version
			{
				get
				{
					return this.m_HeadBytes[5];
				}
				set
				{
					this.m_HeadBytes[5] = value;
				}
			}

			// Token: 0x17000003 RID: 3
			// (get) Token: 0x0600000B RID: 11 RVA: 0x00002468 File Offset: 0x00001468
			// (set) Token: 0x0600000C RID: 12 RVA: 0x0000249C File Offset: 0x0000149C
			public ushort Channels
			{
				get
				{
					return BitConverter.ToUInt16(new byte[]
					{
						this.m_HeadBytes[13],
						this.m_HeadBytes[12]
					}, 0);
				}
				set
				{
					byte[] bytes = BitConverter.GetBytes(value);
					this.m_HeadBytes[13] = bytes[1];
					this.m_HeadBytes[12] = bytes[0];
				}
			}

			// Token: 0x17000004 RID: 4
			// (get) Token: 0x0600000D RID: 13 RVA: 0x000024C8 File Offset: 0x000014C8
			// (set) Token: 0x0600000E RID: 14 RVA: 0x00002514 File Offset: 0x00001514
			public uint Height
			{
				get
				{
					return BitConverter.ToUInt32(new byte[]
					{
						this.m_HeadBytes[17],
						this.m_HeadBytes[16],
						this.m_HeadBytes[15],
						this.m_HeadBytes[14]
					}, 0);
				}
				set
				{
					byte[] bytes = BitConverter.GetBytes(value);
					this.m_HeadBytes[17] = bytes[0];
					this.m_HeadBytes[16] = bytes[1];
					this.m_HeadBytes[15] = bytes[2];
					this.m_HeadBytes[14] = bytes[3];
				}
			}

			// Token: 0x17000005 RID: 5
			// (get) Token: 0x0600000F RID: 15 RVA: 0x00002558 File Offset: 0x00001558
			// (set) Token: 0x06000010 RID: 16 RVA: 0x000025A4 File Offset: 0x000015A4
			public uint Width
			{
				get
				{
					return BitConverter.ToUInt32(new byte[]
					{
						this.m_HeadBytes[21],
						this.m_HeadBytes[20],
						this.m_HeadBytes[19],
						this.m_HeadBytes[18]
					}, 0);
				}
				set
				{
					byte[] bytes = BitConverter.GetBytes(value);
					this.m_HeadBytes[21] = bytes[0];
					this.m_HeadBytes[20] = bytes[1];
					this.m_HeadBytes[19] = bytes[2];
					this.m_HeadBytes[18] = bytes[3];
				}
			}

			// Token: 0x17000006 RID: 6
			// (get) Token: 0x06000011 RID: 17 RVA: 0x000025E8 File Offset: 0x000015E8
			// (set) Token: 0x06000012 RID: 18 RVA: 0x0000261C File Offset: 0x0000161C
			public ushort BitsPerPixel
			{
				get
				{
					return BitConverter.ToUInt16(new byte[]
					{
						this.m_HeadBytes[23],
						this.m_HeadBytes[22]
					}, 0);
				}
				set
				{
					byte[] bytes = BitConverter.GetBytes(value);
					this.m_HeadBytes[23] = bytes[1];
					this.m_HeadBytes[22] = bytes[0];
				}
			}

			// Token: 0x17000007 RID: 7
			// (get) Token: 0x06000013 RID: 19 RVA: 0x00002648 File Offset: 0x00001648
			// (set) Token: 0x06000014 RID: 20 RVA: 0x0000267C File Offset: 0x0000167C
			public ushort ColorMode
			{
				get
				{
					return BitConverter.ToUInt16(new byte[]
					{
						this.m_HeadBytes[25],
						this.m_HeadBytes[24]
					}, 0);
				}
				set
				{
					byte[] bytes = BitConverter.GetBytes(value);
					this.m_HeadBytes[25] = bytes[1];
					this.m_HeadBytes[24] = bytes[0];
				}
			}

			// Token: 0x06000015 RID: 21 RVA: 0x000026A8 File Offset: 0x000016A8
			public PSDHEAD()
			{
				this.m_HeadBytes[0] = 56;
				this.m_HeadBytes[1] = 66;
				this.m_HeadBytes[2] = 80;
				this.m_HeadBytes[3] = 83;
				this.m_HeadBytes[5] = 1;
				this.ColorMode = 3;
				this.BitsPerPixel = 8;
			}

			// Token: 0x04000006 RID: 6
			private byte[] m_HeadBytes = new byte[26];
		}

		// Token: 0x02000004 RID: 4
		private class ColorModel
		{
			// Token: 0x17000008 RID: 8
			// (get) Token: 0x06000016 RID: 22 RVA: 0x00002708 File Offset: 0x00001708
			// (set) Token: 0x06000017 RID: 23 RVA: 0x00002780 File Offset: 0x00001780
			public ColorPalette ColorData
			{
				get
				{
					Bitmap bitmap = new Bitmap(1, 1, PixelFormat.Format8bppIndexed);
					ColorPalette palette = bitmap.Palette;
					if (this.m_ColorData.Length == 0)
					{
						return palette;
					}
					for (int num = 0; num != 256; num++)
					{
						palette.Entries[num] = Color.FromArgb((int)this.m_ColorData[num], (int)this.m_ColorData[num + 256], (int)this.m_ColorData[num + 512]);
					}
					return palette;
				}
				set
				{
					this.m_ColorData = new byte[768];
					for (int num = 0; num != 256; num++)
					{
						this.m_ColorData[num] = value.Entries[num].R;
						this.m_ColorData[num + 256] = value.Entries[num].G;
						this.m_ColorData[num + 512] = value.Entries[num].B;
					}
				}
			}

			// Token: 0x17000009 RID: 9
			// (get) Token: 0x06000018 RID: 24 RVA: 0x00002804 File Offset: 0x00001804
			// (set) Token: 0x06000019 RID: 25 RVA: 0x0000284C File Offset: 0x0000184C
			public uint BIMSize
			{
				get
				{
					return BitConverter.ToUInt32(new byte[]
					{
						this.m_BIMSize[3],
						this.m_BIMSize[2],
						this.m_BIMSize[1],
						this.m_BIMSize[0]
					}, 0);
				}
				set
				{
					byte[] bytes = BitConverter.GetBytes(value);
					this.m_BIMSize[0] = bytes[3];
					this.m_BIMSize[1] = bytes[2];
					this.m_BIMSize[2] = bytes[1];
					this.m_BIMSize[3] = bytes[0];
				}
			}

			// Token: 0x0600001A RID: 26 RVA: 0x0000288C File Offset: 0x0000188C
			public ColorModel(FileStream p_FileStream)
			{
				byte[] array = new byte[4];
				p_FileStream.Read(array, 0, 4);
				Array.Reverse(array);
				int num = BitConverter.ToInt32(array, 0);
				this.m_ColorData = new byte[num];
				if (num != 0)
				{
					p_FileStream.Read(this.m_ColorData, 0, num);
				}
				p_FileStream.Read(this.m_BIMSize, 0, 4);
			}

			// Token: 0x0600001B RID: 27 RVA: 0x000028F7 File Offset: 0x000018F7
			public ColorModel()
			{
				this.m_ColorData = new byte[0];
			}

			// Token: 0x0600001C RID: 28 RVA: 0x00002918 File Offset: 0x00001918
			public byte[] GetBytes()
			{
				MemoryStream memoryStream = new MemoryStream();
				byte[] bytes = BitConverter.GetBytes(this.m_ColorData.Length);
				Array.Reverse(bytes);
				memoryStream.Write(bytes, 0, bytes.Length);
				memoryStream.Write(this.m_ColorData, 0, this.m_ColorData.Length);
				memoryStream.Write(this.m_BIMSize, 0, 4);
				return memoryStream.ToArray();
			}

			// Token: 0x04000007 RID: 7
			private byte[] m_ColorData;

			// Token: 0x04000008 RID: 8
			private byte[] m_BIMSize = new byte[4];
		}

		// Token: 0x02000005 RID: 5
		private class BIM
		{
			// Token: 0x1700000A RID: 10
			// (get) Token: 0x0600001D RID: 29 RVA: 0x00002974 File Offset: 0x00001974
			// (set) Token: 0x0600001E RID: 30 RVA: 0x000029A8 File Offset: 0x000019A8
			public ushort TypeID
			{
				get
				{
					return BitConverter.ToUInt16(new byte[]
					{
						this.m_TypeID[1],
						this.m_TypeID[0]
					}, 0);
				}
				set
				{
					byte[] bytes = BitConverter.GetBytes(value);
					this.m_TypeID[0] = bytes[1];
					this.m_TypeID[1] = bytes[0];
				}
			}

			// Token: 0x0600001F RID: 31 RVA: 0x000029DC File Offset: 0x000019DC
			public BIM(FileStream p_FileStream)
			{
				byte[] array = new byte[4];
				p_FileStream.Read(array, 0, 4);
				if (this.m_Data[0] == array[0] && this.m_Data[1] == array[1] && this.m_Data[2] == array[2] && this.m_Data[3] == array[3])
				{
					p_FileStream.Read(this.m_TypeID, 0, 2);
					int num = p_FileStream.ReadByte();
					int num2 = num;
					if (num2 > 0)
					{
						if (num2 % 2 != 0)
						{
							num = p_FileStream.ReadByte();
						}
						this.m_Name = new byte[num2];
						p_FileStream.Read(this.m_Name, 0, num2);
					}
					num = p_FileStream.ReadByte();
					byte[] array2 = new byte[4];
					p_FileStream.Read(array2, 0, 4);
					Array.Reverse(array2);
					int num3 = BitConverter.ToInt32(array2, 0);
					if (num3 % 2 != 0)
					{
						num3++;
					}
					this.m_Value = new byte[num3];
					p_FileStream.Read(this.m_Value, 0, num3);
					this.m_Read = true;
				}
			}

			// Token: 0x1700000B RID: 11
			// (get) Token: 0x06000020 RID: 32 RVA: 0x00002B09 File Offset: 0x00001B09
			// (set) Token: 0x06000021 RID: 33 RVA: 0x00002B11 File Offset: 0x00001B11
			public bool Read
			{
				get
				{
					return this.m_Read;
				}
				set
				{
					this.m_Read = value;
				}
			}

			// Token: 0x1700000C RID: 12
			// (get) Token: 0x06000022 RID: 34 RVA: 0x00002B1C File Offset: 0x00001B1C
			// (set) Token: 0x06000023 RID: 35 RVA: 0x00002B50 File Offset: 0x00001B50
			public ushort hRes
			{
				get
				{
					return BitConverter.ToUInt16(new byte[]
					{
						this.m_Value[1],
						this.m_Value[0]
					}, 0);
				}
				set
				{
					byte[] bytes = BitConverter.GetBytes(value);
					this.m_Value[0] = bytes[1];
					this.m_Value[1] = bytes[0];
				}
			}

			// Token: 0x1700000D RID: 13
			// (get) Token: 0x06000024 RID: 36 RVA: 0x00002B7C File Offset: 0x00001B7C
			// (set) Token: 0x06000025 RID: 37 RVA: 0x00002BC4 File Offset: 0x00001BC4
			public uint hResUnit
			{
				get
				{
					return BitConverter.ToUInt32(new byte[]
					{
						this.m_Value[5],
						this.m_Value[4],
						this.m_Value[3],
						this.m_Value[2]
					}, 0);
				}
				set
				{
					byte[] bytes = BitConverter.GetBytes(value);
					this.m_Value[2] = bytes[3];
					this.m_Value[3] = bytes[2];
					this.m_Value[4] = bytes[1];
					this.m_Value[5] = bytes[0];
				}
			}

			// Token: 0x1700000E RID: 14
			// (get) Token: 0x06000026 RID: 38 RVA: 0x00002C04 File Offset: 0x00001C04
			// (set) Token: 0x06000027 RID: 39 RVA: 0x00002C38 File Offset: 0x00001C38
			public ushort widthUnit
			{
				get
				{
					return BitConverter.ToUInt16(new byte[]
					{
						this.m_Value[7],
						this.m_Value[6]
					}, 0);
				}
				set
				{
					byte[] bytes = BitConverter.GetBytes(value);
					this.m_Value[6] = bytes[1];
					this.m_Value[7] = bytes[0];
				}
			}

			// Token: 0x1700000F RID: 15
			// (get) Token: 0x06000028 RID: 40 RVA: 0x00002C64 File Offset: 0x00001C64
			// (set) Token: 0x06000029 RID: 41 RVA: 0x00002C98 File Offset: 0x00001C98
			public ushort vRes
			{
				get
				{
					return BitConverter.ToUInt16(new byte[]
					{
						this.m_Value[9],
						this.m_Value[8]
					}, 0);
				}
				set
				{
					byte[] bytes = BitConverter.GetBytes(value);
					this.m_Value[8] = bytes[1];
					this.m_Value[9] = bytes[0];
				}
			}

			// Token: 0x17000010 RID: 16
			// (get) Token: 0x0600002A RID: 42 RVA: 0x00002CC4 File Offset: 0x00001CC4
			// (set) Token: 0x0600002B RID: 43 RVA: 0x00002D10 File Offset: 0x00001D10
			public uint vResUnit
			{
				get
				{
					return BitConverter.ToUInt32(new byte[]
					{
						this.m_Value[13],
						this.m_Value[12],
						this.m_Value[11],
						this.m_Value[10]
					}, 0);
				}
				set
				{
					byte[] bytes = BitConverter.GetBytes(value);
					this.m_Value[10] = bytes[3];
					this.m_Value[11] = bytes[2];
					this.m_Value[12] = bytes[1];
					this.m_Value[13] = bytes[0];
				}
			}

			// Token: 0x17000011 RID: 17
			// (get) Token: 0x0600002C RID: 44 RVA: 0x00002D54 File Offset: 0x00001D54
			// (set) Token: 0x0600002D RID: 45 RVA: 0x00002D88 File Offset: 0x00001D88
			public ushort heightUnit
			{
				get
				{
					return BitConverter.ToUInt16(new byte[]
					{
						this.m_Value[15],
						this.m_Value[14]
					}, 0);
				}
				set
				{
					byte[] bytes = BitConverter.GetBytes(value);
					this.m_Value[14] = bytes[1];
					this.m_Value[15] = bytes[0];
				}
			}

			// Token: 0x04000009 RID: 9
			private byte[] m_Data = new byte[]
			{
				56,
				66,
				73,
				77
			};

			// Token: 0x0400000A RID: 10
			private byte[] m_TypeID = new byte[2];

			// Token: 0x0400000B RID: 11
			private byte[] m_Name = new byte[0];

			// Token: 0x0400000C RID: 12
			public byte[] m_Value;

			// Token: 0x0400000D RID: 13
			private bool m_Read;
		}

		// Token: 0x02000006 RID: 6
		private class LayerMaskInfo
		{
			// Token: 0x0600002E RID: 46 RVA: 0x00002DB4 File Offset: 0x00001DB4
			public LayerMaskInfo(FileStream p_Stream)
			{
				byte[] array = new byte[4];
				p_Stream.Read(array, 0, 4);
				Array.Reverse(array);
				int num = BitConverter.ToInt32(array, 0);
				this.m_Data = new byte[num];
				if (num != 0)
				{
					p_Stream.Read(this.m_Data, 0, num);
				}
			}

			// Token: 0x0600002F RID: 47 RVA: 0x00002E10 File Offset: 0x00001E10
			public LayerMaskInfo()
			{
			}

			// Token: 0x06000030 RID: 48 RVA: 0x00002E24 File Offset: 0x00001E24
			public byte[] GetBytes()
			{
				MemoryStream memoryStream = new MemoryStream();
				byte[] bytes = BitConverter.GetBytes(this.m_Data.Length);
				Array.Reverse(bytes);
				memoryStream.Write(bytes, 0, bytes.Length);
				if (this.m_Data.Length != 0)
				{
					memoryStream.Write(this.m_Data, 0, this.m_Data.Length);
				}
				return memoryStream.ToArray();
			}

			// Token: 0x0400000E RID: 14
			public byte[] m_Data = new byte[0];
		}

		// Token: 0x02000007 RID: 7
		private class ImageData
		{
			// Token: 0x06000031 RID: 49 RVA: 0x00002E7B File Offset: 0x00001E7B
			public ImageData()
			{
			}

			// Token: 0x06000032 RID: 50 RVA: 0x00002E84 File Offset: 0x00001E84
			public ImageData(FileStream p_FileStream, ImagePsd.PSDHEAD p_HeaderInfo)
			{
				this.m_HeaderInfo = p_HeaderInfo;
				byte[] array = new byte[2];
				p_FileStream.Read(array, 0, 2);
				Array.Reverse(array);
				this.p_Type = BitConverter.ToUInt16(array, 0);
				switch (this.p_Type)
				{
				case 0:
					this.RawData(p_FileStream);
					return;
				case 1:
					this.RleData(p_FileStream);
					return;
				default:
					throw new Exception("Type =" + this.p_Type.ToString());
				}
			}

			// Token: 0x06000033 RID: 51 RVA: 0x00002F04 File Offset: 0x00001F04
			private void RleData(FileStream p_Stream)
			{
				switch (this.m_HeaderInfo.ColorMode)
				{
				case 3:
					this.LoadRLERGB(p_Stream);
					return;
				case 4:
					this.LoadRLECMYK(p_Stream);
					return;
				default:
					throw new Exception("RLE ColorMode =" + this.m_HeaderInfo.ColorMode.ToString());
				}
			}

			// Token: 0x06000034 RID: 52 RVA: 0x00002F64 File Offset: 0x00001F64
			private void LoadRLERGB(FileStream p_Stream)
			{
				int width = (int)this.m_HeaderInfo.Width;
				int height = (int)this.m_HeaderInfo.Height;
				this.m_PSDImage = new Bitmap(width, height, PixelFormat.Format24bppRgb);
				BitmapData bitmapData = this.m_PSDImage.LockBits(new Rectangle(0, 0, this.m_PSDImage.Width, this.m_PSDImage.Height), ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);
				byte[] array = new byte[bitmapData.Stride * bitmapData.Height];
				int i = 0;
				int num = bitmapData.Stride * bitmapData.Height;
				p_Stream.Position += (long)(height * (int)this.m_HeaderInfo.Channels * 2);
				int num2 = width * height;
				while (i <= num - 1)
				{
					byte b = (byte)p_Stream.ReadByte();
					if (b != 128)
					{
						if (b > 128)
						{
							b ^= byte.MaxValue;
							b += 2;
							byte b2 = (byte)p_Stream.ReadByte();
							for (byte b3 = 0; b3 != b; b3 += 1)
							{
								switch (i / num2)
								{
								case 0:
								{
									int num3 = i / width;
									int num4 = i % width;
									int num5 = bitmapData.Stride * num3 + num4 * 3 + 2;
									array[num5] = b2;
									break;
								}
								case 1:
								{
									int num3 = (i - num2) / width;
									int num4 = (i - num2) % width;
									int num5 = bitmapData.Stride * num3 + num4 * 3 + 1;
									array[num5] = b2;
									break;
								}
								case 2:
								{
									int num3 = (i - num2 - num2) / width;
									int num4 = (i - num2 - num2) % width;
									int num5 = bitmapData.Stride * num3 + num4 * 3;
									array[num5] = b2;
									break;
								}
								}
								i++;
							}
						}
						else
						{
							b += 1;
							for (byte b4 = 0; b4 != b; b4 += 1)
							{
								switch (i / num2)
								{
								case 0:
								{
									int num3 = i / width;
									int num4 = i % width;
									int num5 = bitmapData.Stride * num3 + num4 * 3 + 2;
									array[num5] = (byte)p_Stream.ReadByte();
									break;
								}
								case 1:
								{
									int num3 = (i - num2) / width;
									int num4 = (i - num2) % width;
									int num5 = bitmapData.Stride * num3 + num4 * 3 + 1;
									array[num5] = (byte)p_Stream.ReadByte();
									break;
								}
								case 2:
								{
									int num3 = (i - num2 - num2) / width;
									int num4 = (i - num2 - num2) % width;
									int num5 = bitmapData.Stride * num3 + num4 * 3;
									array[num5] = (byte)p_Stream.ReadByte();
									break;
								}
								}
								i++;
							}
						}
					}
				}
				Marshal.Copy(array, 0, bitmapData.Scan0, array.Length);
				this.m_PSDImage.UnlockBits(bitmapData);
			}

			// Token: 0x06000035 RID: 53 RVA: 0x00003220 File Offset: 0x00002220
			private void LoadRLECMYK(FileStream p_Stream)
			{
				int width = (int)this.m_HeaderInfo.Width;
				int height = (int)this.m_HeaderInfo.Height;
				int num = width * height * (int)(this.m_HeaderInfo.BitsPerPixel / 8) * (int)this.m_HeaderInfo.Channels;
				p_Stream.Position += (long)(height * (int)this.m_HeaderInfo.Channels * 2);
				byte[] array = new byte[num];
				int i = 0;
				while (i <= num - 1)
				{
					byte b = (byte)p_Stream.ReadByte();
					if (b != 128)
					{
						if (b > 128)
						{
							b ^= byte.MaxValue;
							b += 2;
							byte b2 = (byte)p_Stream.ReadByte();
							for (byte b3 = 0; b3 != b; b3 += 1)
							{
								array[i] = b2;
								i++;
							}
						}
						else
						{
							b += 1;
							for (byte b4 = 0; b4 != b; b4 += 1)
							{
								array[i] = (byte)p_Stream.ReadByte();
								i++;
							}
						}
					}
				}
				this.m_PSDImage = new Bitmap(width, height, PixelFormat.Format24bppRgb);
				BitmapData bitmapData = this.m_PSDImage.LockBits(new Rectangle(0, 0, this.m_PSDImage.Width, this.m_PSDImage.Height), ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);
				byte[] array2 = new byte[bitmapData.Stride * bitmapData.Height];
				int num2 = width * height;
				double num3 = Math.Pow(2.0, (double)this.m_HeaderInfo.BitsPerPixel);
				int num4 = num2 * 2;
				int num5 = num2 * 3;
				for (int num6 = 0; num6 != bitmapData.Height; num6++)
				{
					int num7 = bitmapData.Stride * num6;
					int num8 = num6 * width;
					for (int num9 = 0; num9 != bitmapData.Width; num9++)
					{
						double p_C = 1.0 - (double)array[num8 + num9] / num3;
						double p_M = 1.0 - (double)array[num8 + num9 + num2] / num3;
						double p_Y = 1.0 - (double)array[num8 + num9 + num4] / num3;
						double p_K = 1.0 - (double)array[num8 + num9 + num5] / num3;
						this.ConvertCMYKToRGB(p_C, p_M, p_Y, p_K, array2, num7 + num9 * 3);
					}
				}
				Marshal.Copy(array2, 0, bitmapData.Scan0, array2.Length);
				this.m_PSDImage.UnlockBits(bitmapData);
			}

			// Token: 0x06000036 RID: 54 RVA: 0x0000348C File Offset: 0x0000248C
			private void RawData(FileStream p_Stream)
			{
				switch (this.m_HeaderInfo.ColorMode)
				{
				case 2:
					this.LoadRAWIndex(p_Stream);
					return;
				case 3:
					this.LoadRAWRGB(p_Stream);
					return;
				case 4:
					this.LoadRAWCMYK(p_Stream);
					return;
				default:
					throw new Exception("RAW ColorMode =" + this.m_HeaderInfo.ColorMode.ToString());
				}
			}

			// Token: 0x06000037 RID: 55 RVA: 0x000034F8 File Offset: 0x000024F8
			private void LoadRAWCMYK(FileStream p_Stream)
			{
				int width = (int)this.m_HeaderInfo.Width;
				int height = (int)this.m_HeaderInfo.Height;
				this.m_PSDImage = new Bitmap(width, height, PixelFormat.Format24bppRgb);
				BitmapData bitmapData = this.m_PSDImage.LockBits(new Rectangle(0, 0, this.m_PSDImage.Width, this.m_PSDImage.Height), ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);
				byte[] array = new byte[bitmapData.Stride * bitmapData.Height];
				int num = (int)(this.m_HeaderInfo.BitsPerPixel / 8);
				int num2 = width * height;
				int num3 = num2 * 4 * num;
				byte[] array2 = new byte[num3];
				p_Stream.Read(array2, 0, num3);
				int num4 = width * height;
				double num5 = Math.Pow(2.0, (double)this.m_HeaderInfo.BitsPerPixel);
				int num6 = num4 * 2;
				int num7 = num4 * 3;
				if (num == 2)
				{
					num4 *= 2;
					num6 *= 2;
					num7 *= 2;
				}
				for (int num8 = 0; num8 != bitmapData.Height; num8++)
				{
					int num9 = bitmapData.Stride * num8;
					int num10 = num8 * width;
					if (num == 2)
					{
						num10 *= 2;
					}
					for (int num11 = 0; num11 != bitmapData.Width; num11++)
					{
						switch (num)
						{
						case 1:
						{
							double p_C = 1.0 - (double)array2[num10 + num11] / num5;
							double p_M = 1.0 - (double)array2[num10 + num11 + num4] / num5;
							double p_Y = 1.0 - (double)array2[num10 + num11 + num6] / num5;
							double p_K = 1.0 - (double)array2[num10 + num11 + num7] / num5;
							this.ConvertCMYKToRGB(p_C, p_M, p_Y, p_K, array, num9 + num11 * 3);
							break;
						}
						case 2:
						{
							double p_C = 1.0 - (double)BitConverter.ToUInt16(array2, num10 + num11 * 2) / num5;
							double p_M = 1.0 - (double)BitConverter.ToUInt16(array2, num10 + num11 * 2 + num4) / num5;
							double p_Y = 1.0 - (double)BitConverter.ToUInt16(array2, num10 + num11 * 2 + num6) / num5;
							double p_K = 1.0 - (double)BitConverter.ToUInt16(array2, num10 + num11 * 2 + num7) / num5;
							this.ConvertCMYKToRGB(p_C, p_M, p_Y, p_K, array, num9 + num11 * 3);
							break;
						}
						}
					}
				}
				Marshal.Copy(array, 0, bitmapData.Scan0, array.Length);
				this.m_PSDImage.UnlockBits(bitmapData);
			}

			// Token: 0x06000038 RID: 56 RVA: 0x00003790 File Offset: 0x00002790
			private void LoadRAWIndex(FileStream p_Stream)
			{
				int width = (int)this.m_HeaderInfo.Width;
				int height = (int)this.m_HeaderInfo.Height;
				this.m_PSDImage = new Bitmap(width, height, PixelFormat.Format8bppIndexed);
				BitmapData bitmapData = this.m_PSDImage.LockBits(new Rectangle(0, 0, this.m_PSDImage.Width, this.m_PSDImage.Height), ImageLockMode.ReadWrite, PixelFormat.Format8bppIndexed);
				byte[] array = new byte[bitmapData.Stride * bitmapData.Height];
				int num = width * height;
				byte[] array2 = new byte[num];
				p_Stream.Read(array2, 0, num);
				int num2 = 0;
				for (int num3 = 0; num3 != height; num3++)
				{
					int num4 = num3 * bitmapData.Stride;
					for (int num5 = 0; num5 != width; num5++)
					{
						array[num5 + num4] = array2[num2];
						num2++;
					}
				}
				Marshal.Copy(array, 0, bitmapData.Scan0, array.Length);
				this.m_PSDImage.UnlockBits(bitmapData);
			}

			// Token: 0x06000039 RID: 57 RVA: 0x00003884 File Offset: 0x00002884
			private void LoadRAWRGB(FileStream p_Stream)
			{
				int width = (int)this.m_HeaderInfo.Width;
				int height = (int)this.m_HeaderInfo.Height;
				this.m_PSDImage = new Bitmap(width, height, PixelFormat.Format24bppRgb);
				BitmapData bitmapData = this.m_PSDImage.LockBits(new Rectangle(0, 0, this.m_PSDImage.Width, this.m_PSDImage.Height), ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);
				byte[] array = new byte[bitmapData.Stride * bitmapData.Height];
				int num = width * height;
				int num2 = num * 3 * (int)(this.m_HeaderInfo.BitsPerPixel / 8);
				byte[] array2 = new byte[num2];
				p_Stream.Read(array2, 0, num2);
				int num3 = 0;
				int num4 = num;
				int num5 = num + num;
				int num6 = 0;
				if (this.m_HeaderInfo.BitsPerPixel == 16)
				{
					num4 *= (int)(this.m_HeaderInfo.BitsPerPixel / 8);
					num5 *= (int)(this.m_HeaderInfo.BitsPerPixel / 8);
				}
				for (int num7 = 0; num7 != height; num7++)
				{
					int num8 = num7 * bitmapData.Stride;
					for (int num9 = 0; num9 != width; num9++)
					{
						array[num9 * 3 + 2 + num8] = array2[num6 + num3];
						array[num9 * 3 + 1 + num8] = array2[num6 + num4];
						array[num9 * 3 + num8] = array2[num6 + num5];
						num6 += (int)(this.m_HeaderInfo.BitsPerPixel / 8);
					}
				}
				Marshal.Copy(array, 0, bitmapData.Scan0, array.Length);
				this.m_PSDImage.UnlockBits(bitmapData);
			}

			// Token: 0x17000012 RID: 18
			// (get) Token: 0x0600003A RID: 58 RVA: 0x00003A02 File Offset: 0x00002A02
			// (set) Token: 0x0600003B RID: 59 RVA: 0x00003A0A File Offset: 0x00002A0A
			public Bitmap PSDImage
			{
				get
				{
					return this.m_PSDImage;
				}
				set
				{
					this.m_PSDImage = value;
				}
			}

			// Token: 0x0600003C RID: 60 RVA: 0x00003A14 File Offset: 0x00002A14
			private void ConvertCMYKToRGB(double p_C, double p_M, double p_Y, double p_K, byte[] p_DataBytes, int p_Index)
			{
				int num = (int)((1.0 - (p_C * (1.0 - p_K) + p_K)) * 255.0);
				int num2 = (int)((1.0 - (p_M * (1.0 - p_K) + p_K)) * 255.0);
				int num3 = (int)((1.0 - (p_Y * (1.0 - p_K) + p_K)) * 255.0);
				if (num < 0)
				{
					num = 0;
				}
				else if (num > 255)
				{
					num = 255;
				}
				if (num2 < 0)
				{
					num2 = 0;
				}
				else if (num2 > 255)
				{
					num2 = 255;
				}
				if (num3 < 0)
				{
					num3 = 0;
				}
				else if (num3 > 255)
				{
					num3 = 255;
				}
				p_DataBytes[p_Index] = (byte)num3;
				p_DataBytes[p_Index + 1] = (byte)num2;
				p_DataBytes[p_Index + 2] = (byte)num;
			}

			// Token: 0x0400000F RID: 15
			private ushort p_Type;

			// Token: 0x04000010 RID: 16
			private ImagePsd.PSDHEAD m_HeaderInfo;

			// Token: 0x04000011 RID: 17
			private Bitmap m_PSDImage;
		}
	}
}
