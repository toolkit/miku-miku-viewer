﻿/*
 * 由SharpDevelop创建。
 * 用户： Administrator
 * 日期: 2022/1/5/周三
 * 时间: 10:36
 * 
 * 要改变这种模板请点击 工具|选项|代码编写|编辑标准头文件
 */
using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;

namespace MikuMikuViewer
{
	/// <summary>
	/// Description of FlipImage.
	/// </summary>
	public class FlipImage
	{
		/// <summary>
		/// 图像水平翻转
		/// </summary>
		/// <param name="bmp">原来图像</param>
		/// <returns></returns>
		public static Bitmap HorizontalFlip(Bitmap bmp)
		{
		try
		{
		var width = bmp.Width;
		var height = bmp.Height;
		Graphics g = Graphics.FromImage(bmp);
		Rectangle rect = new Rectangle(0, 0, width, height);
		bmp.RotateFlip(RotateFlipType.RotateNoneFlipX);
		g.DrawImage(bmp, rect);
		return bmp;
		}
		catch (Exception ex)
		{
		return bmp;
		}
		 
		}
		 
		 
		/// <summary>
		/// 图像垂直翻转
		/// </summary>
		/// <param name="bit">原来图像</param>
		/// <returns></returns>
		public static Bitmap VerticalFlip(Bitmap bmp)
		{
		try
		{
		var width = bmp.Width;
		var height = bmp.Height;
		Graphics g = Graphics.FromImage(bmp);
		Rectangle rect = new Rectangle(0, 0, width, height);
		bmp.RotateFlip(RotateFlipType.RotateNoneFlipY);
		g.DrawImage(bmp, rect);
		return bmp;
		}
		catch (Exception ex)
		{
		return bmp;
		}
		}
		
		
		public static Bitmap Rotate(Bitmap b, float angle)
		{
		angle = angle % 360; //弧度转换
		double radian = angle * Math.PI / 180.0;
		double cos = Math.Cos(radian);
		double sin = Math.Sin(radian);
		//原图的宽和高
		int w = b.Width;
		int h = b.Height;
		int W = (int)(Math.Max(Math.Abs(w * cos - h * sin), Math.Abs(w * cos + h * sin)));
		int H = (int)(Math.Max(Math.Abs(w * sin - h * cos), Math.Abs(w * sin + h * cos)));
		//目标位图
		Bitmap dsImage = new Bitmap(W, H);
		System.Drawing.Graphics g = System.Drawing.Graphics.FromImage(dsImage);
		g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.Bilinear;
		g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
		//计算偏移量
		Point Offset = new Point((W - w) / 2, (H - h) / 2);
		//构造图像显示区域：让图像的中心与窗口的中心点一致
		Rectangle rect = new Rectangle(Offset.X, Offset.Y, w, h);
		Point center = new Point(rect.X + rect.Width / 2, rect.Y + rect.Height / 2);
		g.TranslateTransform(center.X, center.Y);
		g.RotateTransform(360 - angle);
		
		//恢复图像在水平和垂直方向的平移
		g.TranslateTransform(-center.X, -center.Y);
		g.DrawImage(b, rect);
		//重至绘图的所有变换
		g.ResetTransform();
		g.Save();
		g.Dispose();
		return dsImage;
		}
		//***************************
	}
}
